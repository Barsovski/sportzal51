<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "trainers".
 *
 * @property int $id
 * @property string $name
 * @property string $about
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 */
class Trainers extends \yii\db\ActiveRecord
{
    const IMAGE_PATH = '/uploads/trainers/';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trainers';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'about'], 'required'],
            [['created_at', 'updated_at', 'on_main'], 'integer'],
            [['about', 'alias'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 9],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxSize' => 1024*1024*8],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'alias' => 'Синоним',
            'about' => 'Описание',
            'image' => 'Картинка',
            'color' => 'Цвет расписания',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
            'on_main' => 'На главной',
        ];
    }

    public function fileUrl()
    {
        $file = $this->image;
        $url = Url::to('@web' . self::IMAGE_PATH . $file );
        return $url;
    }
}
