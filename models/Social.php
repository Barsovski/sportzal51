<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;

/**
 * This is the model class for table "social".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $url
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 */
class Social extends \yii\db\ActiveRecord
{
    const IMAGE_PATH = '/uploads/social/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'alias', 'url'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxSize' => 1024*1024*8],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'url' => 'Ссылка',
            'image' => 'Иконка',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
        ];
    }

    public function fileUrl()
    {
        $file = $this->image;
        $url = Url::to('@web' . self::IMAGE_PATH . $file );
        return $url;
    }
}
