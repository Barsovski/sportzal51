<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ScheduleForm extends Model
{
    public $sport_id;
    public $trainer_id;
    public $hall;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['sport_id', 'trainer_id', 'hall'], 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'sport_id' => 'Вид спорта',
            'trainer_id' => 'Тренер',
            'hall' => 'Зал',
        ];
    }

}
