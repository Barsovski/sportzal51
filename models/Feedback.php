<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property int $opened
 * @property string $contact
 * @property string $name
 * @property string $body
 * @property int $created_at
 * @property int $updated_at
 */
class Feedback extends \yii\db\ActiveRecord
{
    public $captcha;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['opened'], 'integer'],
            [['contact', 'name', 'body'], 'required'],
            [['contact', 'name', 'body'], 'string', 'max' => 255],
            ['captcha', 'captcha', 'when' => function($model){
                return $model->getIsNewRecord();
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'opened' => 'Просмотрено',
            'contact' => 'Контактные данные',
            'name' => 'Имя',
            'body' => 'Обращение',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
            'captcha' => 'Капча',
        ];
    }
}
