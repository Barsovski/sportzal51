<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;

/**
 * This is the model class for table "menu_items".
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property int $list_order
 * @property int $parent
 * @property int $created_at
 * @property int $updated_at
 *
 * @property MenuItems $parent0
 * @property MenuItems[] $menuItems
 */
class MenuItems extends \yii\db\ActiveRecord
{
    public function getParent()
    {
        return $this->hasOne(MenuItems::className(), ['id' => 'parent_id'])
                    ->from(self::tableName() . ' mi2');
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_items}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'list_order'], 'required'],
            [['list_order', 'parent_id'], 'integer'],
            [['name', 'link', 'alias'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItems::className(), 'targetAttribute' => ['parent' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'link' => 'Ссылка',
            'list_order' => 'Порядок',
            'parent_id' => 'Родитель',
            'alias' => 'Alias',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(MenuItems::className(), ['id' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(MenuItems::className(), ['parent' => 'id']);
    }

    /**
     * Return TRUE is menu item has a child
     */
    public function hasChildIn($arr)
    {
        $checker = false;
        foreach ($arr as $item) {
            if($item->parent && $item->parent_id == $this->id){
                $checker = true;
                break 1;
            }
        }

        return $checker;
    }
}
