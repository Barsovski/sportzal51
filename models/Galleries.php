<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "galleries".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $list_order
 * @property int $visible
 * @property int $created_at
 * @property int $updated_at
 *
 * @property GalleriesImage[] $galleriesImages
 */
class Galleries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'galleries';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['list_order', 'visible'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'list_order' => 'Порядок',
            'visible' => 'Видимость',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleriesImages()
    {
        return $this->hasMany(GalleriesImages::className(), ['parent_id' => 'id']);
    }

    public function createTimeToFormat($format='')
    {
        $time = $this->created_at;
        if(empty($format))
            $format = 'Y.m.d h:i:s';

        return @date($format, $time);
    }
}
