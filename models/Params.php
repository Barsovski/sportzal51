<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "params".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $val_text
 * @property string $val_file
 * @property int $created_at
 * @property int $updated_at
 */
class Params extends \yii\db\ActiveRecord
{
    const FILE_PATH = '/uploads/params/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'params';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'alias', 'val_text'], 'string', 'max' => 255],
            [['val_file'], 'file', 'skipOnEmpty' => true, 'maxSize' => 1024*1024*8],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'val_text' => 'Значение текст',
            'val_file' => 'Значение файл',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
        ];
    }

    public function isImageFile()
    {
        $check_arr = ['jpeg', 'jpg', 'png', 'gif', 'svg'];
        $check = false;
        if($this->val_file)
        {
            foreach ($check_arr as $key => $value) {
                if( strpos($this->val_file, '.'.$value) !== false ){
                    $check = true;
                    break 1;
                }
            }
        }
        
        return $check;
    }

    public function fileUrl()
    {
        $file = $this->val_file;
        $url = Url::to('@web' . self::FILE_PATH . $file );
        return $url;
    }
}
