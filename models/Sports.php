<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;

use app\models\Trainers;

/**
 * This is the model class for table "sports".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $about
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 */
class Sports extends \yii\db\ActiveRecord
{
    const IMAGE_PATH = '/uploads/sports/';

    public function getTrainer()
    {
        return $this->hasOne(Trainers::className(), ['id' => 'trainer_id']);
                    // ->from(self::tableName() . ' AS trainer');
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sports}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['about'], 'string'],
            [['trainer_id', 'on_main'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 9],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxSize' => 1024*1024*8],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'about' => 'Описание',
            'image' => 'Картинка',
            'trainer_id' => 'Тренер',
            'color' => 'Цвет расписания',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
            'on_main' => 'На главной',
        ];
    }

    public function fileUrl()
    {
        $file = $this->image;
        $url = Url::to('@web' . self::IMAGE_PATH . $file );
        return $url;
    }
}
