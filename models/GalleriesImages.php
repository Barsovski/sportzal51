<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "galleries_images".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $image
 * @property int $visible
 *
 * @property Gallery $parent
 */
class GalleriesImages extends \yii\db\ActiveRecord
{
    const FILE_PATH = '/uploads/galleries-images/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'galleries_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'visible'], 'integer'],
            [['name', 'parent_id'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'maxSize' => 1024*1024*8],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Galleries::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Галерея',
            'name' => 'Название/описание',
            'image' => 'Изображение',
            'visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Galleries::className(), ['id' => 'parent_id']);
    }

    /**
     * Remove image file
     */
    public function afterDelete()
    {
        parent::afterDelete();
        if( $this->image!=='' ){
            $FILES_FOLDER = Yii::getAlias('@webroot/' . $this::FILE_PATH);
            @unlink($FILES_FOLDER . $this->image);
        }
    }

    public function fileUrl()
    {
        $file = $this->image;
        $url = Url::to('@web' . self::FILE_PATH . $file );
        return $url;
    }

}
