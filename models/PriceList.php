<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "price_list".
 *
 * @property int $id
 * @property int $price_period_id
 * @property string $name
 * @property string $text
 * @property string $price
 * @property int $list_order
 * @property int $visible
 *
 * @property PricePeriods $pricePeriod
 */
class PriceList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_period_id', 'name', 'text', 'price'], 'required'],
            [['price_period_id', 'list_order', 'visible'], 'integer'],
            [['name', 'text', 'price'], 'string', 'max' => 255],
            [['price_period_id'], 'exist', 'skipOnError' => true, 'targetClass' => PricePeriods::className(), 'targetAttribute' => ['price_period_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_period_id' => 'Цены - период',
            'name' => 'Название',
            'text' => 'Текст',
            'price' => 'Стоимость',
            'list_order' => 'Порядок',
            'visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricePeriod()
    {
        return $this->hasOne(PricePeriods::className(), ['id' => 'price_period_id']);
    }
}
