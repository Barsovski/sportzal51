<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $image
 * @property string $name
 * @property string $body
 * @property string $alias
 * @property int $created_at
 * @property int $updated_at
 */
class News extends \yii\db\ActiveRecord
{
    const FILE_PATH = '/uploads/news/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['body'], 'string'],
            [['visible'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxSize' => 1024*1024*8],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'name' => 'Название',
            'body' => 'Текст новости',
            'alias' => 'Alias',
            'visible' => 'Видимость',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
        ];
    }

    public function fileUrl()
    {
        $file = $this->image;
        $url = Url::to('@web' . self::FILE_PATH . $file );
        return $url;
    }

    public function createTimeToFormat($format='')
    {
        $time = $this->created_at;
        if(empty($format))
            $format = 'Y.m.d h:i:s';

        return @date($format, $time);
    }
}
