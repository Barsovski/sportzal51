<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\Url;

use app\models\Sports;
use app\models\Trainers;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property string $date
 * @property string $time_s
 * @property string $time_e
 * @property string $info
 * @property int $sport
 * @property int $trainer
 * @property int $created_at
 * @property int $updated_at
 */
class Schedule extends \yii\db\ActiveRecord
{
    const HALLS = array(
        ''=> '', 
        1  => 'Большой зал', 
        2 => 'Малый зал' 
    );
    const HALLS_FILTER = array(
        1  => 'Большой зал', 
        2 => 'Малый зал' 
    );
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function getSport()
    {
        return $this->hasOne(Sports::className(), ['id' => 'sport_id']);
    }

    public function getTrainer()
    {
        return $this->hasOne(Trainers::className(), ['id' => 'trainer_id']);
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'time_s', 'time_e', 'sport_id'], 'required'],
            [['date', 'time_s', 'time_e', 'info'], 'safe'],
            [['sport_id', 'trainer_id', 'hall'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'День недели',
            'time_s' => 'Время начала',
            'time_e' => 'Время окончания',
            'sport_id' => 'Вид спорта',
            'info' => 'Пояснение',
            'trainer_id' => 'Тренер',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
            'hall' => 'Зал',
        ];
    }
}
