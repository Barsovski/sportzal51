<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "price_periods".
 *
 * @property int $id
 * @property string $name
 * @property int $list_order
 * @property int $visible
 *
 * @property PriceList[] $priceLists
 */
class PricePeriods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_periods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['list_order', 'visible'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'list_order' => 'Порядок',
            'visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceLists()
    {
        return $this->hasMany(PriceList::className(), ['price_period_id' => 'id']);
    }
}
