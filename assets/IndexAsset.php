<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/app_news.css",
        "css/bs3_styles.css",
        "css/page_main.css",
        "css/app_main.css",
        "css/swiper.css",
	    "css/jquery.mmenu.all.css",
        "css/menu-theme.css",
        "css/page_about_trainer.css",
        "css/page_about_sport.css",
        "vendors/lightbox/css/lightbox.min.css",
    ];
    public $js = [
        "js/jquery.lazy.min.js",
	    "js/jquery.maskedinput.min.js",
        "js/jquery.scrollbar.js",
        "js/jquery.mhead.js",
	    "js/tabs.js",
        "js/jquery.mmenu.all.js",
        "vendors/lightbox/js/lightbox.min.js",
        "js/main.js",
        "js/lazyloads.js",
        "js/playground.js",
        "js/mmenu.js",
    ];
    public $depends = [
        "yii\web\YiiAsset",
        "yii\bootstrap\BootstrapAsset",
        "yii\bootstrap\BootstrapPluginAsset",
    ];
}

