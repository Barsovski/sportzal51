<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%menu_items}}`.
 */
class m190822_082855_create_menu_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menu_items}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'link' => $this->string()->null(),
            'list_order' => $this->integer()->default(0)->notNull(),
            'parent' => $this->integer()->default(0)->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
        ]);
        $this->addForeignKey(
            'parent_obj',
            '{{%menu_items}}',
            'parent',
            '{{%menu_items}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menu_items}}');
        $this->dropForeignKey(
              'parent_obj',
              '{{%menu_items}}'
          );
    }
}
