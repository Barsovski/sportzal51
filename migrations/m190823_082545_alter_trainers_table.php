<?php

use yii\db\Migration;

/**
 * Class m190823_082545_alter_trainers_table
 */
class m190823_082545_alter_trainers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%trainers}}', 'about', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190823_082545_alter_trainers_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190823_082545_alter_trainers_table cannot be reverted.\n";

        return false;
    }
    */
}
