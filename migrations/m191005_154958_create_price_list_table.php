<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%price_list}}`.
 */
class m191005_154958_create_price_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%price_list}}', [
            'id' => $this->primaryKey(),
            'price_period_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'text' => $this->string()->notNull(),
            'price' => $this->string()->notNull(),
            'list_order' => $this->integer()->defaultValue(0)->notNull(),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%price_list}}');
    }
}
