<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%sports}}`.
 */
class m190826_220150_add_trainer_column_to_sports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sports', 'trainer_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sports', 'trainer_id');
    }
}
