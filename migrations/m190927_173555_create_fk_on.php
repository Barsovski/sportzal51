<?php

use yii\db\Migration;

/**
 * Class m190927_173555_create_fk_on
 */
class m190927_173555_create_fk_on extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'parent_obj',
            '{{%galleries_images}}',
            'parent_id',
            '{{%galleries}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190927_173555_create_fk_on cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_173555_create_fk_on cannot be reverted.\n";

        return false;
    }
    */
}
