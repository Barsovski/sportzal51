<?php

use yii\db\Migration;

/**
 * Class m190827_134904_alter_date_on_schedule_table
 */
class m190827_134904_alter_date_on_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%schedule}}', 'date', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190827_134904_alter_date_on_schedule_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190827_134904_alter_date_on_schedule_table cannot be reverted.\n";

        return false;
    }
    */
}
