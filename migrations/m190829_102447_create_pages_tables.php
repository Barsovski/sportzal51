<?php

use yii\db\Migration;

/**
 * Class m190829_102447_create_pages_tables
 */
class m190829_102447_create_pages_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pages}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->null(),
            'body' => $this->text()->notNull(),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pages}}');
    }

}
