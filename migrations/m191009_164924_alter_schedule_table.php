<?php

use yii\db\Migration;

/**
 * Class m191009_164924_alter_schedule_table
 */
class m191009_164924_alter_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('schedule}}', 'color', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('schedule', 'color');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191009_164924_alter_schedule_table cannot be reverted.\n";

        return false;
    }
    */
}
