<?php

use yii\db\Migration;

/**
 * Class m190829_202945_rename_fields_schedule_table
 */
class m190829_202945_rename_fields_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('schedule', 'sport', 'sport_id');
        $this->renameColumn('schedule', 'trainer', 'trainer_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190829_202945_rename_fields_schedule_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190829_202945_rename_fields_schedule_table cannot be reverted.\n";

        return false;
    }
    */
}
