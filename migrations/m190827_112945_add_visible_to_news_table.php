<?php

use yii\db\Migration;

/**
 * Class m190827_112945_add_visible_to_news_table
 */
class m190827_112945_add_visible_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('news', 'visible', $this->boolean()->notNull()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('news', 'visible');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190827_112945_add_visible_to_news_table cannot be reverted.\n";

        return false;
    }
    */
}
