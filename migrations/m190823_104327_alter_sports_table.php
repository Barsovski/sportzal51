<?php

use yii\db\Migration;

/**
 * Class m190823_104327_alter_sports_table
 */
class m190823_104327_alter_sports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%sports}}', 'about', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190823_104327_alter_sports_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190823_104327_alter_sports_table cannot be reverted.\n";

        return false;
    }
    */
}
