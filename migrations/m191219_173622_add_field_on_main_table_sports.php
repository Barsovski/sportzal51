<?php

use yii\db\Migration;

/**
 * Class m191219_173622_add_field_on_main_table_sports
 */
class m191219_173622_add_field_on_main_table_sports extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sports', 'on_main', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('trainers', 'on_main', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191219_173622_add_field_on_main_table_sports cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191219_173622_add_field_on_main_table_sports cannot be reverted.\n";

        return false;
    }
    */
}
