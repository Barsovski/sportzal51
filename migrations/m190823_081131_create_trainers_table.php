<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%trainers}}`.
 */
class m190823_081131_create_trainers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%trainers}}');
        
        $this->createTable('{{%trainers}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'about' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%trainers}}');
    }
}
