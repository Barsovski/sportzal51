<?php

use yii\db\Migration;

/**
 * Class m191217_173956_alter_schedule_table_add_hall_column
 */
class m191217_173956_alter_schedule_table_add_hall_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('schedule', 'hall', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191217_173956_alter_schedule_table_add_hall_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191217_173956_alter_schedule_table_add_hall_column cannot be reverted.\n";

        return false;
    }
    */
}
