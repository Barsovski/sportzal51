<?php

use yii\db\Migration;

/**
 * Class m191009_164925_alter_schedule_table
 */
class m191009_164925_alter_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('schedule', 'color', 'info');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('schedule', 'info', 'color');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191009_164925_alter_schedule_table cannot be reverted.\n";

        return false;
    }
    */
}
