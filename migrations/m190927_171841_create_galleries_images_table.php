<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%galleries_images}}`.
 */
class m190927_171841_create_galleries_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%galleries_images}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->defaultValue(0)->null(),
            'name' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%galleries_images}}');
    }
}
