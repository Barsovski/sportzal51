<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sports}}`.
 */
class m190823_103415_create_sports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sports}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'alias' => $this->string()->null(),
            'about' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sports}}');
    }
}
