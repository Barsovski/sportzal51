<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%schedule}}`.
 */
class m190826_181217_create_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%schedule}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->notNull(),
            'time_s' => $this->time()->notNull(),
            'time_e' => $this->time()->notNull(),
            'sport' => $this->integer()->defaultValue(0)->null(),
            'trainer' => $this->integer()->defaultValue(0)->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%schedule}}');
    }
}
