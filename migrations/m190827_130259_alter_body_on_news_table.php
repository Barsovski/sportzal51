<?php

use yii\db\Migration;

/**
 * Class m190827_130259_alter_body_on_news_table
 */
class m190827_130259_alter_body_on_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%news}}', 'body', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190827_130259_alter_body_on_news_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190827_130259_alter_body_on_news_table cannot be reverted.\n";

        return false;
    }
    */
}
