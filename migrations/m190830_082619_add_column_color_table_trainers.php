<?php

use yii\db\Migration;

/**
 * Class m190830_082619_add_column_color_table_trainers
 */
class m190830_082619_add_column_color_table_trainers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('trainers', 'color', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190830_082619_add_column_color_table_trainers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190830_082619_add_column_color_table_trainers cannot be reverted.\n";

        return false;
    }
    */
}
