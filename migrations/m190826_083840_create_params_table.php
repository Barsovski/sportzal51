<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%params}}`.
 */
class m190826_083840_create_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%params}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'alias' => $this->string()->null(),
            'val_text' => $this->string()->null(),
            'val_file' => $this->string()->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%params}}');
    }
}
