<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%trainers}}`.
 */
class m190826_231410_add_alias_column_to_trainers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('trainers', 'alias', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('trainers', 'alias');
    }
}
