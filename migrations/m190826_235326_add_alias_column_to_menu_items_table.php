<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%menu_items}}`.
 */
class m190826_235326_add_alias_column_to_menu_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu_items', 'alias', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('menu_items', 'alias');
    }
}
