<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%price_periods}}`.
 */
class m191005_153817_create_price_periods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%price_periods}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'list_order' => $this->integer()->defaultValue(0)->notNull(),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%price_periods}}');
    }
}
