<?php

use yii\db\Migration;

/**
 * Class m191109_104012_add_field_color_table_sports
 */
class m191109_104012_add_field_color_table_sports extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sports', 'color', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191109_104012_add_field_color_table_sports cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191109_104012_add_field_color_table_sports cannot be reverted.\n";

        return false;
    }
    */
}
