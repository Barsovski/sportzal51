<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = $model->created_at . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Обратная связь', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'opened',
                'value' => function($model){
                    if($model->opened == 1)
                        return 'Просмотрено';
                    else
                        return 'Новый';
                    
                }
            ],
            'contact',
            'name',
            'body',
            'created_at:datetime',
            // 'updated_at',
        ],
    ]) ?>

</div>
