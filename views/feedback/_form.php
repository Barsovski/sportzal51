<?php

use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */

$form_id = 'contact-form';
$pjax_id = 'pjax-'.$form_id;
?>
<?php Pjax::begin([
    'id'=>$pjax_id,
    'enablePushState'=>false,
]) ?>

    <div class="feedback-form">

        <?php $form = ActiveForm::begin([
            'id' => 'contact-form',
            'action'=> ['feedback/create'],
            'enableAjaxValidation'=>true,
            'enableClientValidation' => false,
            'options' => [
                'data-pjax' => true,
            ]
        ]); ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <?= $form->field($model, 'contact')->textInput(['maxlength' => true, 'class'=>'']) ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class'=>'']) ?>
                </div>
            </div>
            <div class="col-xs-12">
                <?= $form->field($model, 'body')->textArea(['class'=>'mess']) ?>
            </div>

            <div class="col-xs-12">
                <?= $form->field($model, 'captcha')->widget(Captcha::classname(), []) ?>
            </div>

        </div>
        
        <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="col-xs-12">
            <?php echo Yii::$app->session->getFlash('success') ?>
        </div>
        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-danger btn-sm']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php Pjax::end() ?>