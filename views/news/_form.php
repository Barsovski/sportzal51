<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= $form->field($model, 'image')->fileInput() ?>
        <?php
            if($model->image){
                echo Html::img(
                        Html::encode(
                            Url::to('@web' . $model::FILE_PATH . $model->image )
                        ), ['style'=>'max-height: 200px;']
                    );
            }
        ?>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->widget(\yii2jodit\JoditWidget::className()) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'visible')->DropDownList([1 => 'Показывать', 0 => 'Не показывать']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
