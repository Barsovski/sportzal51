<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = 'Список новостей';
\yii\web\YiiAsset::register($this);

?>
<div class="section section-top">
    <div class="wrap">
        <div class="row">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'layout'=>"{items}\n<div class='col-xs-12'>{pager}</div>",
                'options'=>[
                    'style'=>'width: 100%;',
                    'class' => 'news-list',
                    'id' => false
                ],
                'itemOptions' => [
                    'tag' => false,
                ],
            ]); ?>
        </div>
                                    
    </div>
</div>
<div class="wrap"></div>