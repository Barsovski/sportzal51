<?php
use yii\helpers\Url;
?>
<div class="col-xs-6 col-sm-6 col-md-3 news-item">
    <div class="news-box bg-black">
        <div class="news-img">
            <a href="<?= Url::toRoute(['news/show', 'alias'=>$model->alias]) ?>">
                <img class="lazy" data-src="<?= $model->fileUrl() ?>" alt="<?= $model->name ?>">
            </a>
        </div>
        <div class="news-date"><?= $model->createTimeToFormat('Y.m.d') ?></div>
        <div class="news-name"><?= $model->name ?></div>
        <a class="news-link btn btn-danger" href="<?= Url::toRoute(['news/show', 'alias'=>$model->alias]) ?>">Подробнее</a>
    </div>
</div>