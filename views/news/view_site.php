<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды спорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="section">
    <div class="wrap">
        <div class="section-page ">
            <div class="row">
                <div class="col-xs-12">
                    <div class="h3 mt-5"><?= $model->name ?></div>
                </div>
                
                <div class="col-xs-12">
                    <div class="row">
                        <!-- <div class=""> -->
                            <div class="text-center col-xs-12 col-md-4 col-lg-4">
                                <img class="lazy width-100 news-image-left" data-src="<?= $model->fileUrl() ?>">
                            </div>
                        <!-- </div> -->
                        <div class="news-detail ma-15">
                            <div class="news-detail-txt">
                                <?= $model->body ?>
                            </div>
                            <strong class="news-date"><?= $model->createTimeToFormat('Y.m.d'); ?></strong>
                            <hr>
                            <div class="text-left">
                                <a class="btn btn-danger" href="<?= Url::toRoute(['news/list']) ?>">Возврат к списку новостей</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>