<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Schedule;

/* @var $this yii\web\View */
/* @var $model app\models\Schedule */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Расписание', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="schedule-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно собираетесь удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'time_s',
            'time_e',
            [
                'attribute'=>'sport_id',
                'value'=>function($model){
                    return @$model->sport->name;
                }
            ],
            [
                'attribute'=>'trainer_id',
                'value'=>function($model){
                    return @$model->trainer->name;
                }
            ],
            [
                'attribute'=>'hall',
                'value'=>function($model){
                    return Schedule::HALLS[$model->hall];
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
