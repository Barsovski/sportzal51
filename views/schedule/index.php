<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\models\Sports;
use app\models\Trainers;
use app\models\Schedule;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$days = [
    1=>'Понедельник',
    2=>'Вторник',
    3=>'Среда',
    4=>'Четверг',
    5=>'Пятница',
    6=>'Суббота',
    7=>'Воскресенье'
];

$this->title = 'Расписание';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'date',
                'value'=>function($model) use ($days){
                    if($model->date)
                        return @$days[$model->date];
                },
                'filter'=>$days,
            ],
            'time_s',
            'time_e',
            [
                'attribute'=>'sport_id',
                'value'=>'sport.name',
                'filter'=>ArrayHelper::map(Sports::find()->all(),'id','name'),
            ],
            [
                'attribute'=>'hall',
                'value'=>function($model){
                    return Schedule::HALLS[$model->hall];
                },
                'filter'=> Schedule::HALLS_FILTER,
            ],
            [
                'attribute'=>'trainer_id',
                'value'=>'trainer.name',
                'filter'=>ArrayHelper::map(Trainers::find()->all(),'id','name'),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
