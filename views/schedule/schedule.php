<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;

use yii\widgets\ActiveForm;

use app\models\Schedule;

// use app\models\Schedule;
$this->title = 'Расписание';
?>
<div class="section section-top">
    <div class="wrap flex-top">

        <div class="col-xs-12 text-center">
            <div class="h1"><?= Html::encode($this->title) ?></div>
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'schedule_timetable_form',
            'action'=> ['schedule/page'],
            'enableAjaxValidation'=>false,
            'enableClientValidation' => false,
            'options' => [
                'data-pjax' => true,
                'class'=>'form-style',
            ]
        ]); ?>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <?= $form->field($filter_form, 'sport_id')->dropDownList(
                        ArrayHelper::map($sports, 'id','name'),
                        ['prompt'=>'Выберите вид спорта', 'class'=>'form-control']
                    ) ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <?= $form->field($filter_form, 'trainer_id')->dropDownList(
                        ArrayHelper::map($trainers, 'id','name'),
                        ['prompt'=>'Выберите тренера', 'class'=>'form-control']
                    ) ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <?= $form->field($filter_form, 'hall')->dropDownList(
                        Schedule::HALLS_FILTER,
                        ['prompt'=>'Выберите зал', 'class'=>'form-control']
                    ) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
        <!-- <form id="schedule_timetable_form" class="shedulefilter" method="POST">
            <button class="btn btn-black" id="clear">Сбросить</button>
		</form> -->

        <div class="section-scroll-y">

            <?= $this->render('_schedule_block', [
                'days'=>$days,
                'times_array'=>$times_array,
                'schedules'=>$schedules,
            ]) ?>

        </div>
    </div>
</div>