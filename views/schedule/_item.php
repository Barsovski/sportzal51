<?php
use yii\helpers\Url;
use yii\helpers\Html;

$color = '';
if(!$item->isNewRecord){
    if($item->sport_id && $item->sport && $item->sport->color && $item->sport->color!==''){
        $color = 'background: '.$item->sport->color;
    }
}
?>

<div class="timetable-item" style="<?= @$color ?>">
    <!-- <a data-fancybox="" data-type="ajax" data-src="/schedule/ajax.php?eid=267" href="javascript:;"> -->
    <a style="" href="javascript:;">
        <div class="timetable-item-time">
            <?php
                if(strtotime($item->time_s)){
                echo date('H:i', strtotime($item->time_s));
                }
                echo ' - ';
                if(strtotime($item->time_e)){
                    echo date('H:i', strtotime($item->time_e));
                }
            ?>
        </div>
        <div class="timetable-item-discipline">
            <?php 
                if($item->sport_id && $item->sport)
                    echo Html::a($item->sport->name, Url::toRoute(['sports/show', 'alias'=>$item->sport->alias]));
                else
                    echo '&nbsp;';
            ?>
        </div>
        <div class="timetable-item-discipline">
            <?php 
                if($item->info && $item->info !== '') 
                    echo $item->info;
                else
                    echo '&nbsp;';
            ?>
        </div>
        <div class="timetable-item-trener">
            <?php 
                if($item->trainer){
                    $name_ = $item->trainer->name;
                    $name_ = explode(' ', $name_);
                    $tname = [];
                    if(count($name_)>=2)
                        foreach ($name_ as $key => $value) {
                            if($key==0){
                                array_push($tname, $value, ' ');
                            }
                            else
                                if( strtoupper(mb_substr($value, 0, 1))!=='' )
                                    array_push($tname, strtoupper(mb_substr($value, 0, 1)).'.');
                        }
                    else
                        array_push($tname, $item->trainer->name);

                    echo Html::a(implode($tname), Url::toRoute(['trainers/show', 'alias'=>$item->trainer->alias, 'style'=>'overflow: hidden;']));
                }
                else {
                    echo '&nbsp;';
                }
            ?>
            <div class="timetable-item-hall" style="font-size: 12px;"><?= $hall ? $hall : '&nbsp;' ?></div>
        </div>
    </a>
</div>