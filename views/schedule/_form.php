<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

use app\models\Sports;
use app\models\Trainers;
use app\models\Schedule;

/* @var $this yii\web\View */
/* @var $model app\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->DropDownList([
            1 => 'Понедельник', 
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье',
        ])
    ?>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            
            <?= $form->field($model, 'time_s')->widget(MaskedInput::className(), [
                'clientOptions'=>[
                    'alias'=>'hh:mm',
                ]
            ]) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'time_e')->widget(MaskedInput::className(), [
                'clientOptions'=>[
                    'alias'=>'hh:mm',
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?= $form->field($model, 'info'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'sport_id')->dropDownList(
                ArrayHelper::map(Sports::find()->all(),'id','name'),
                ['prompt'=>'Выберите вид спорта']
            ) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'trainer_id')->dropDownList(
                ArrayHelper::map(Trainers::find()->all(),'id','name'),
                ['prompt'=>'Выберите тренера']
            ) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?= $form->field($model, 'hall')->dropDownList(Schedule::HALLS); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
