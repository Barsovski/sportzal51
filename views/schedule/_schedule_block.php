<?php
use app\models\Schedule;
use yii\widgets\Pjax;

$model_empty = new Schedule();
?>
<?php Pjax::begin([
    'id'=>'schedule_timetable',
    'enablePushState'=>false,
    'formSelector' => '#schedule_timetable_form',
    'submitEvent' => 'change',
]) ?>
<div class="timetable">
    <?php 
        // листаем дни недели
        foreach ($days as $day=>$day_name) {
            echo '<div class="timetable-col">';
            echo '  <div class="week-name">'. $day_name .'</div>';
            // листаем массив времени
            foreach ($times_array as $time_line) {
                $check = $model_empty;
                foreach ($schedules as $item) {
                    if($item->date == $day && $item->time_s == $time_line){
                        $check = $item;
                    }
                }
                if($check->isNewRecord){
                    echo $this->render('_item', ['item'=>$check, 'hall'=>null, 'class'=>'empty']);
                } else {
                    echo $this->render('_item', ['item'=>$check, 'hall'=>Schedule::HALLS[$check->hall]]);
                }
            }
            // листаем занятия
            echo '</div>';
        }
    ?>
                    
</div>
<?php Pjax::end() ?>