body {
    <?php if($color_body): ?> color: <?=$color_body->val_text ?>;
    <?php endif;
    ?><?php if($color_main_bg): ?> background: <?=$color_main_bg->val_text;
    ?>;
    <?php endif;
    ?>
}

<?php if($color_a): ?>a {
    color: <?=$color_a->val_text ?>;
}

<?php endif;
?>.trener-item {
    border-radius: 1px;
    <?php if($trener_border): ?> border: 2px solid <?=$trener_border->val_text ?>;
    <?php endif;
    ?><?php if($trener_bg): ?> background: <?=$trener_bg->val_text ?>;
    <?php endif;
    ?>
}

<?php if($trener_color): ?>.trener-item .trener-name {
    color: <?=$trener_color->val_text ?>;
}

<?php endif;
?>.news-box {
    border-radius: 1px;
    <?php if($news_border): ?> border: 2px solid <?=$news_border->val_text ?>;
    <?php endif;
    ?><?php if($news_bg): ?> background: <?=$news_bg->val_text ?>;
    <?php endif;
    ?><?php if($news_color): ?> color: <?=$news_color->val_text ?>;
    <?php endif;
    ?>
}

<?php if($sport_color): ?>.news-item-name-v2 {
    color: <?=$sport_color->val_text ?>;
}

<?php endif;
?><?php if($color_title): ?>.title-page {
    color: <?=$color_title->val_text ?>;
}

<?php endif;
?><?php if($color_nav_a): ?>.nav ul li ul li a {
    color: <?=$color_nav_a->val_text ?> !important;
}

<?php endif;
?><?php if($color_timetable): ?>.timetable-item {
    background: <?=$color_timetable->val_text ?>;
}

<?php endif;
?><?php if($color_timetable_font): ?>.timetable-item a {
    color: <?=$color_timetable_font->val_text ?>;
}

.timetable-item-discipline {
    color: <?=$color_timetable_font->val_text ?>;
}

<?php endif;
?>.price-item-box {
    <?php if($price_color_back): ?> background: <?=$price_color_back->val_text ?>;
    <?php endif;
    ?><?php if($price_color_text): ?> color: <?=$price_color_text->val_text ?>;
    <?php endif;
    ?><?php if($price_color_border): ?> border: 2px solid <?=$price_color_border->val_text ?>;
    <?php endif;
    ?>
}

body {
    <?php if($color_body): ?> color: <?=$color_body->val_text ?>;
    <?php endif ?><?php if($color_main_bg): ?> background-color: <?=$color_main_bg->val_text;
    ?>;
    <?php endif ?><?php if($main_bg): ?> background-image: url(<?= $main_bg->fileUrl();
    ?>);
    <?php endif ?>background-repeat: no-repeat;
    background-position: bottom left;
    background-size: cover;
    background-attachment: fixed;
}

footer {
    <?php if($color_footer): ?> background-color: <?=$color_footer->val_text;
    ?>;
    <?php endif;
    ?><?php if($footer_text_size): ?> font-size: <?=$footer_text_size->val_text;
    ?>px;
    <?php endif;
    ?>
}

header {
    background: <?=$header_color_bg->val_text;
    ?>
}

.btn-header {
    color: <?=$main_color_adv->val_text ?>
}

.nav ul li a {
    color: <?=$header_color_main_link->val_text ?>
}

.nav ul li a:hover,
header nav ul li.active a {
    color: <?=$main_color_adv->val_text ?>;
}

.nav ul li ul {
    background: <?=$main_color_adv->val_text ?>;
}

.nav ul li ul .triangle::after {
    border-left: 200px solid <?=$main_color_adv->val_text ?>;
    border-top: 0 solid <?=$main_color_adv->val_text ?>;
}

.section-title::after {
    background: <?=$main_color_adv->val_text ?>
}

.section-title a:hover {
    border-color: <?=$main_color_adv->val_text ?>
}

.red {
    color: <?=$main_color_adv->val_text ?>
}

.tabs__caption2 li.active {
    color: <?=$main_color_adv->val_text ?>
}

.week-name {
    background: <?=$main_color_adv->val_text ?>;
    color: <?=$color_timetable_wd->val_text ?>;
}