<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ParamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Параметры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="params-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить параметр', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'alias',
            [
                'label' => 'Значение',
                'format' => 'raw',
                'value' => function($model){
                    if( $model->val_file ){
                        if( $model->isImageFile()){
                            $string = Html::img(
                                    Html::decode(
                                        Url::to('@web' . $model::FILE_PATH . $model->val_file )
                                    ), ['style'=>'height: 20px;']
                                );
                        }
                        else{
                            $string = Html::a($model->val_file,
                                    Html::decode(
                                        Url::to('@web' . $model::FILE_PATH . $model->val_file )
                                    )
                                );
                        }
                        if($model->val_text)
                            $string .= '<br>'.$model->val_text;
                        return $string;
                    } 
                    else{
                        // return strpos($model->val_text, '#');
                        if(strpos($model->val_text, '#')!==false && strpos($model->val_text, '#')==0)
                            return '<div class="color-block-inline" style="background:'. $model->val_text .'"></div>'.$model->val_text;
                        else
                            return $model->val_text;
                    }
                        
                },
                'filter' => false,
                'contentOptions' => ['nowrap'=>''],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
