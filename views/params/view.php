<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Params */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Параметры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="params-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно собираетесь удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'alias',
            [
                'label' => 'Значение',
                'format' => 'raw',
                'value' => function($model){
                    if( $model->val_file ){
                        if( $model->isImageFile()){
                            $string = Html::img(
                                    Html::decode(
                                        Url::to('@web' . $model::FILE_PATH . $model->val_file )
                                    ), ['style'=>'height: 20px;']
                                );
                        }
                        else{
                            $string = Html::a($model->val_file,
                                    Html::decode(
                                        Url::to('@web' . $model::FILE_PATH . $model->val_file )
                                    )
                                );
                        }
                        if($model->val_text)
                            $string .= '<br>'.$model->val_text;
                        return $string;
                    } 
                    else{
                        // return strpos($model->val_text, '#');
                        if(strpos($model->val_text, '#')!==false && strpos($model->val_text, '#')==0)
                            return '<div class="color-block-inline" style="background:'. $model->val_text .'"></div>'.$model->val_text;
                        else
                            return $model->val_text;
                    }
                        
                },
                'filter' => false,
                'contentOptions' => ['nowrap'=>''],
            ],
            [
                'attribute' => 'val_file',
                'format' => 'raw',
                'value' => function($model){
                    if( $model->val_file ){
                        if( $model->isImageFile()){
                            return Html::img(
                                    Html::decode(
                                        Url::to('@web' . $model::FILE_PATH . $model->val_file )
                                    ), ['style'=>'max-height: 200px;']
                                );
                        }
                        else{
                            return Html::a($model->val_file,
                                    Html::decode(
                                        Url::to('@web' . $model::FILE_PATH . $model->val_file )
                                    )
                                );
                        }
                    } 
                    else
                        return '';
                },
                'filter' => false,
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
