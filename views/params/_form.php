<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Params */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="params-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= $form->field($model, 'val_text')->textInput(['maxlength' => true, 'style'=>'border-bottom: 5px solid #CCC'])
                
        ?>
        <button class="btn btn-sm btn-primary btn-xs" id="params-color">Выбрать цвет для поля "Значение текст"</button>
    </div>
    
    <div class="form-group">
        <?= $form->field($model, 'val_file')->fileInput() ?>
                
        <?php
            if($model->val_file){
                if($model->isImageFile()){
                    echo Html::img(
                            Html::encode(
                                Url::to('@web' . $model::FILE_PATH . $model->val_file )
                            ), ['style'=>'max-height: 200px;']
                        );
                }
                else{
                    echo Html::a($model->val_file,
                        Url::to('@web' . $model::FILE_PATH . $model->val_file )
                    );
                }
            }
        ?>
        <div class="alert alert-warning" style="padding: 5px 10px 0;">
            <input id="clearimage" name="clearimage" type="checkbox"> <label for="clearimage">Удалить изображение</label>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <!-- <?php if( $model->id ): ?>
            <?= Html::a('Удалить картинку', Url::to(['params/remove-image', 'id'=>$model->id]), $options = ['class'=>'btn btn-warning']); ?>
        <?php endif ?> -->
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
    $this->registerJs("
        $('#params-color').ColorPicker({
            color: '#0000ff',
            onBeforeShow: function () {
                if( $('#params-val_text').val() )
                    $(this).ColorPickerSetColor($('#params-val_text').val());
                
            },
            onShow: function(colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function(colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function(hsb, hex, rgb) {return false;},
            onSubmit: function(hsb, hex, rgb, el) {
                $('#params-val_text').val('#'+hex);
                $(this).ColorPickerHide();
                $('#params-val_text').trigger('change');
                $('#params-val_text').css('border-bottom-color', $('#params-val_text').val())
                                    .css('border-bottom-width', '5px')
                                    .css('border-bottom-style', 'solid');
                return false;
            },
        });
        $(document).on('change', '#params-val_text', function(){
            $(this).css('border-bottom-color', $(this).val());
        });
        $(document).ready(function(){
            if($('#params-val_text').val()){
                $('#params-val_text').css('border-bottom-color', $('#params-val_text').val());
                $('#params-val_text').trigger('change');
            }
        });
    ", View::POS_END);
?>