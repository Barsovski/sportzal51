<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use app\models\Trainers;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды спорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="section">
    <div class="wrap">

        <div class="section-page">
            <div class="first-section row">
                <div class="col-xs-12 col-md-6" style="padding-bottom: 40px;">
                    <div class="first-text">
                        <p class="h1" style="margin-top: 0;">
                            <?= $model->name ?>
                            <span class="red">Занятия<br>каждую неделю</span>
                        </p>
                        <p class="h3">Узнай даты проведения</p>
                        <a class="btn btn-danger btn-lg" href="<?= Url::toRoute(['schedule/page']) ?>">Расписание</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                <?php if($model->image): ?>
                    <center>
                        <img class="lazy" data-src="<?= $model->fileUrl() ?>" style="max-width: 100%;">
                    </center>
                <?php endif ?>
                </div>
                <div class="content-center col-xs-12" style="margin-top: 40px;">
                    <h1 class="preview-title"><?= $model->name ?> это</h1>
                    <div><?= $model->about ?></div>
                    <hr>
                    <div class="text-left">
                        <a class="btn btn-danger" href="<?= Url::toRoute(['sports/list']) ?>">Возврат к списку дисциплин</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- <div class="section">
    <div class="wrap">

        <div class="section section-discipline section-page">
            <div class="first-section">
                <div class="col-xs-12 col-md-6" style="padding-bottom: 40px;">
                <div class="wrap wrap-full wrap-content flex">
                    <div class="first-text">
                        <p class="h1">
                            <?= $model->name ?>
                            <span class="red">Занятия<br>каждую неделю</span>
                        </p>
                        <p class="h3">Узнай даты проведения</p>
                        <a class="btn btn-danger btn-lg" href="<?= Url::toRoute(['schedule/page']) ?>">Расписание</a>
                    </div>
                </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                <?php if($model->image): ?>
                    <center>
                        <img class="lazy" data-src="<?= $model->fileUrl() ?>" style="max-width: 100%;">
                    </center>
                <?php endif ?>
                </div>
            </div>
        </div>

        <div class="section">
            <div class="wrap wrap-content flex flex-top">
                <div class="content-center">
                    <h1 class="preview-title"><?= $model->name ?> это</h1>
                    <div><?= $model->about ?></div>
                    <hr>
                    <div class="text-center">
                        <a class="btn btn-danger" href="<?= Url::toRoute(['sports/list']) ?>">Возврат к списку дисциплин</a>
                    </div>
                </div>
            </div>
            
        </div>

    </div>
</div> -->