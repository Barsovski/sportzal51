<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\models\Trainers;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Виды спорта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sports-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить вид спорта', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($model){
                    if( $model->image ){
                        return Html::img(
                                Html::decode(
                                    Url::to('@web' . $model::IMAGE_PATH . $model->image )
                                ), ['style'=>'height: 60px;']
                            );
                    } 
                    else
                        return '';
                },
                'filter' => false,
                'headerOptions' => ['style' => 'width: 100px'],
                'contentOptions' => ['style' => 'width: 100px;', 'class'=>'text-center'],
            ],
            'name',
            'alias',
            [
                'attribute'=>'color',
                'value'=>function($model){
                    if($model->color)
                        if($model->color && strpos($model->color, '#')!==false && strpos($model->color, '#')==0){
                            return '<div class="color-block-default" style="background:'. $model->color .'"></div>'.$model->color;
                        }
                        else
                            return $model->color;
                    else
                        return $model->color;
                },
                'format'=>'html',
            ],
            [
                'attribute'=>'trainer_id',
                'value'=>'trainer.name',
                'filter'=>ArrayHelper::map(Trainers::find()->all(),'id','name'),
            ],
            [
                'attribute' => 'on_main',
                'format' => 'raw',
                'value' => function($model){
                    if($model->on_main)
                        return '<span class="glyphicon glyphicon-ok" aria-hidden="false"></span>';
                    else
                        return '';
                },
                'contentOptions' => ['style' => 'width: 100px; font-size: 20px;', 'class'=>'text-center text-info'],
                'filter' => array(1 => 'На главной', 0 => 'Прочие' ),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
