<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды спорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sports-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно собираетесь удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'alias',
            [
                'attribute'=>'color',
                'value'=>function($model){
                    if($model->color)
                        if($model->color && strpos($model->color, '#')!==false && strpos($model->color, '#')==0){
                            return '<div class="color-block-default" style="background:'. $model->color .'"></div>'.$model->color;
                        }
                        else
                            return $model->color;
                    else
                        return $model->color;
                },
                'format'=>'raw',
            ],
            [
                'attribute' => 'trainer_id',
                'value' => function ($model){
                    if($model->trainer_id)
                        return Html::a($model->trainer->name, ['trainers/view', 'id'=>$model->trainer_id]);
                    else
                        return '';
                },
                'filter' => false,
                'format' => ['html'],
            ],
            'about:html',
            [
                'attribute'=>'image',
                'value'=> function($model){
                    if($model->image){
                        return Url::to('@web' . $model::IMAGE_PATH . $model->image );
                    }
                },
                'format' => ['image',['height'=>'200']],
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
