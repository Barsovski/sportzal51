<?php
use yii\helpers\Url;
?>
<div class="news-item-v2">
    <div class="news-item-bg-v2 lazy" data-src="<?= $model->fileUrl() ?>"></div>
    <div class="news-item-name-v2"><?= $model->name ?></div>
    <a class="news-item-link-v2 btn btn-danger btn-sm"
        href="<?= Url::toRoute(['sports/show', 'alias'=>$model->alias]) ?>">Подробнее</a>
</div>