<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use app\models\Trainers;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sports-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'about')->widget(\yii2jodit\JoditWidget::className()) ?>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'trainer_id')->dropDownList(
                ArrayHelper::map(Trainers::find()->all(),'id','name'),
                ['prompt'=>'Выберите тренера']
            ) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'color')->textInput(['maxlength' => true, 'style'=>'border-bottom: 5px solid #CCC']) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'image')->fileInput() ?>
        <?php
            if($model->image){
                echo Html::img(
                        Html::encode(
                            Url::to('@web' . $model::IMAGE_PATH . $model->image )
                        ), ['style'=>'max-height: 200px;']
                    );
            }
        ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'on_main')->checkBox(['selected' => $model->on_main]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
    $this->registerJs("
        let input_name = '#sports-color';
        let input = $('#sports-color');
        input.ColorPicker({
            color: '#0000ff',
            onBeforeShow: function () {
                if( this.value )
                    $(this).ColorPickerSetColor(this.value);
                
            },
            onShow: function(colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function(colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onSubmit: function(hsb, hex, rgb, el) {
                input.val('#'+hex);
                $(el).ColorPickerHide();
                input.trigger('change');
            },
        });
        $(document).on('change', input_name, function(){
            $(this).css('border-bottom-color', $(this).val());
        });
        $(document).ready(function(){
            if(input.val()){
                input.css('border-bottom-color', input.val());
                input.trigger('change');

            }
        });
    ", View::POS_END);
?>
