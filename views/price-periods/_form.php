<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PricePeriods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-periods-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'list_order')->textInput() ?>

    <?= $form->field($model, 'visible')->DropDownList([1 => 'Показывать', 0 => 'Не показывать']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
