<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Inflector;

use app\models\Params;
use app\models\Sports;
use app\models\Trainers;
use app\models\News;
use app\models\Social;

// $bg_main = Params::findOne(['alias'=>'main-bg-1']);
$bg_main2 = Params::findOne(['alias'=>'main-bg-2']);
$logo_main = Params::findOne(['alias'=>'main-big-logo']);
$main_spogan = Params::findOne(['alias'=>'main-slogan']);
$sports = Sports::find()->filterWhere(['sports.on_main'=>1])->joinWith('trainer')->limit(6)->all();
$trainers = Trainers::find()->filterWhere(['trainers.on_main'=>1])->limit(4)->all();
// $logo_main = Params::findOne(['alias'=>'logo-main']);
$contact_phone = Params::findOne(['alias'=>'contact-phone']);
$contact_address = Params::findOne(['alias'=>'contact-address']);

$news = News::find()->where(['visible'=>1])->orderBy(['created_at'=>SORT_DESC])->limit(4)->all();

$c_phones = Params::find()->andFilterWhere(['like', 'alias', 'contact-phone'])->all();
$c_emails = Params::find()->andFilterWhere(['like', 'alias', 'contact-email'])->all();
$c_address = Params::find()->andFilterWhere(['like', 'alias', 'contact-address'])->all();
$c_wates = Params::find()->andFilterWhere(['like', 'alias', 'contact-workdate'])->all();
$c_socials = Social::find()->all();
// $footer_icon = Params::findOne(['alias'=>'footer-icon']);

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<div class="section section-full section-top bg-layout">
    <div class="wrap wrap-full wrap-content first-slider flex">
        <div class="first-text">
            <img class="lazy" data-src="<?= $logo_main->fileUrl() ?>" alt="">
            <p class="h3 h3-index">
                <?= $main_spogan->val_text ?>
            </p>
            <div class="first-text-btn">
                <a class="btn btn-danger btn-lg" href="<?= Url::toRoute(['schedule/page']) ?>">Расписание</a>
            </div>
        </div>
        <img  class="lazy first-trener" data-src="<?= $bg_main2->fileUrl(); ?>">
    </div>
</div>
<div class="wrap">
    <div class="section-title">
        Дисциплины 
        <a href="<?= Url::to(['sports/list']) ?>">Все дисциплины</a>
    </div>
</div>
<div class="section section-full">
    <div class="wrap wrap-full">
        <div class="news-list-v2">
            <?php
                foreach ($sports as $item) {
                    echo $this->render('/sports/_item', ['model'=>$item]);
                }
            ?>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="section-title">
        Тренеры 
        <a href="<?= Url::to(['trainers/list']) ?>">Все тренеры</a>
    </div>
</div>
<div id="team" class="section">
    <div class="wrap">
        <div class="trener-box-list">

            <div class="trener-right" style="width: 100%;">
                
                <?php
                    foreach ($trainers as $item) {
                        echo $this->render('/trainers/_item', ['model'=>$item]);
                    }
                ?>
            </div>


            <div class="clr"></div>
        </div>
    </div>
</div>
<div class="wrap">
    <div class="section-title">
        Новости 
        <a href="<?= Url::toRoute(['news/list']) ?>">Все новости</a>
    </div>
</div>
<div class="section">
    <div class="wrap">
        <div class="row">
            <div class="news-list">

                <?php
                    foreach ($news as $item) {
                        echo $this->render('/news/_item', ['model'=>$item]);
                    }
                ?>
                
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="section-title">
    Информация     
    <?= Html::a('Контакты', Url::to(['site/contact'])) ?>
    </div>
</div>
<div class="section">
    <div class="wrap flex-row">
        <div class="contacts-map">
            <?php 
                if ($this->beginCache('ymap', ['duration' => 5])){
                    echo $this->render('_ymaps');
                    $this->endCache();
                }
            ?>
        </div>
        <div class="contacts-addr">
            
            <?php foreach ($c_wates as $item): ?>
                <div class="contacts-addr-phone">
                    <?= $item->val_text; ?>
                </div>
            <?php endforeach ?>
            <?php
                foreach ($c_wates as $item) {
                    print($item->val_text);
                }
            ?>
            <?php foreach ($c_phones as $item): ?>
                <div class="contacts-addr-addr" style="margin-bottom: 10px;">
                    <?= $item->val_text; ?>
                </div>
            <?php endforeach ?>
            <?php foreach ($c_emails as $item): ?>
                <div class="contacts-addr-addr" style="margin-bottom: 10px;">
                    <?= Html::a($item->val_text, "mailto:$item->val_text") ?>
                </div>
            <?php endforeach ?>
            <?php foreach ($c_address as $item): ?>
                <div class="contacts-addr-addr" style="margin-bottom: 10px;">
                    <?= $item->val_text; ?>
                </div>
            <?php endforeach ?>

            <?php foreach ($c_socials as $item): ?>
                <a href="<?= $item->url; ?>" title="<?= $item->name; ?>" style="width: 40px; height: 40px; display: inline-block;">
                    <?= Html::img(Html::encode(Url::to($item->fileUrl())), ['style'=>'max-width: 100%; max-height: 100%;']); ?>
                </a>
            <?php endforeach ?>
            <div>
                <?= Html::a('Обратная связь', Url::to(['site/feedback']), $options = ['class'=>'btn btn-warning', 'style'=>'margin-top: 20px;']) ?>
            </div>
        </div>
    </div>
</div>