<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<style>
    .wrap.error h1{
        min-height: 200px;
        position: relative;
        transform: translateY(50%);
        text-align: center;
    }
</style>
<div class="section">
    <div class="wrap error">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div class="alert alert-warning">
                    <strong><?= nl2br(Html::encode($message)) ?></strong>
                </div>
                <p>
                    Во время выполнения запроса произошла ошибка. 
                </p>
                <p>
                    В случае повторения ошибки и нарушения работы сайта свяжитесь с нами по указанным на сайте телефонам.
                </p>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                
            </div>
        </div>
    </div>
</div>
