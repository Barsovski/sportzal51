<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
// use yii\widgets\Pjax;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="section section-top">
    <div class="wrap flex-top">
    
        <div class="col-xs-12 text-center">
            <div class="h1"><?= Html::encode($this->title) ?></div>
        </div>

        <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                
            <?= $this->render('/feedback/_form', [
                'model' => $model,
            ]) ?>

        </div>

    </div>
</div>
