<?php
use yii\helpers\Html;
use yii\helpers\Inflector;
/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<div class="site-index">

    <style>
        .fs-big .glyphicon{
            font-size: 30px;
        }
        .fs-big a{
            font-weight: bold;
            font-size: 16px;
        }
    </style>

    <div class="jumbotron">
        <h2>Администрирование</h2>
        <h1>[ <?php echo Yii::$app->user->identity->username ?> ]</h1>
    </div>

    <div class="body-content"></div>

        <div class="row fs-big">
            <div class="col-lg-4 col-sm-6">
                <h3>Меню</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Меню', ['menu-items/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Параметры</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Параметры', ['params/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Социальные сети</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Соц.сети', ['social/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
        </div>
        <div class="row fs-big">
            <div class="col-xs-12">
                <hr>
            </div>
            
            <div class="col-lg-4 col-sm-6">
                <h3>Новости</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Новости', ['news/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Страницы</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Страницы', ['pages/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
        </div>
        <div class="row fs-big">
            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Тренеры</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Тренеры', ['trainers/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Виды спорта</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Виды спорта', ['sports/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Расписание</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Расписание', ['schedule/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
        </div>
        <div class="row fs-big">
            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Галереи - список</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Галереи - список', ['galleries/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Галереи - изображения</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Галереи - изображения', ['galleries-images/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
        </div>
        <div class="row fs-big">
            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Цены - периоды</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Цены - периоды', ['price-periods/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Цены - список цен</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Цены - список цен', ['price-list/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
        </div>
        <div class="row fs-big">
            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-lg-4 col-sm-6">
                <h3>Обратная связь</h3>
                <?= Html::a('<span class="glyphicon glyphicon-th-list"></span><br>Обратная связь', ['feedback/index'], ['class'=>'btn btn-sm btn-block btn-success']); ?>
            </div>
        </div>

    </div>
</div>
