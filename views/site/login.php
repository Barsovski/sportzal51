<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход в систему';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login col-xs-12 col-md-6 col-md-offset-3">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><b>Заполните поля ниже для входа в систему:</b></p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class='col-xs-12'>{input}</div>\n<div class='col-xs-12'>{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class'=>'form-control input-sm']) ?>

        <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control input-sm']) ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-xs-12\">{input} {label}</div>\n<div class=\"col-xs-12\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-xs-12">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-sm', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
