<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\Params;
use app\models\Social;
// use yii\widgets\Pjax;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;

$c_phones = Params::find()->andFilterWhere(['like', 'alias', 'contact-phone'])->all();
$c_emails = Params::find()->andFilterWhere(['like', 'alias', 'contact-email'])->all();
$c_address = Params::find()->andFilterWhere(['like', 'alias', 'contact-address'])->all();
$c_wates = Params::find()->andFilterWhere(['like', 'alias', 'contact-workdate'])->all();
$c_socials = Social::find()->all();

?>

<div class="section section-top">
    <div class="wrap flex-top">

        <div class="col-xs-12 text-center">
            <div class="h1"><?= Html::encode($this->title) ?></div>
        </div>

        <div class="col-xs-12 section bg-black">
            <div class="row">
                
                <div class="col-xs-12 col-md-4">
                    <?php foreach ($c_wates as $item): ?>
                        <div class="contacts-addr-phone">
                            <?= $item->val_text; ?>
                        </div>
                    <?php endforeach ?>
                    <?php
                        foreach ($c_wates as $item) {
                            print($item->val_text);
                        }
                    ?>
                    <center>
                        <span class="red glyphicon glyphicon-circle-arrow-down" style="font-size: 35px;" aria-hidden="true"></span>
                    </center>
                    <div class="h3">Контакты:</div>
                    <hr style="margin: 0 0 20px;">
                    <?php foreach ($c_phones as $item): ?>
                        <div class="contacts-addr-addr" style="margin-bottom: 10px;">
                            <?= $item->val_text; ?>
                        </div>
                    <?php endforeach ?>
                    <?php foreach ($c_emails as $item): ?>
                        <div class="contacts-addr-addr" style="margin-bottom: 10px;">
                            <?= Html::a($item->val_text, "mailto:$item->val_text") ?>
                        </div>
                    <?php endforeach ?>
                </div>

                <div class="col-xs-12 col-md-4">
                    <center><span class="red glyphicon glyphicon-map-marker" style="font-size: 35px;" aria-hidden="true"></span></center>
                    <div class="h3">Адреса:</div>
                    <hr style="margin: 0 0 20px;">
                    <?php foreach ($c_address as $item): ?>
                        <div class="contacts-addr-addr" style="margin-bottom: 10px;">
                            <?= $item->val_text; ?>
                        </div>
                    <?php endforeach ?>
                </div>
                
                <div class="col-xs-12 col-md-4">
                    <center><span class="red glyphicon glyphicon-info-sign" style="font-size: 35px;" aria-hidden="true"></span></center>
                    <div class="h3">Социальные сети:</div>
                    <hr style="margin: 0 0 20px;">
                    <?php foreach ($c_socials as $item): ?>
                        <a href="<?= $item->url; ?>" title="<?= $item->name; ?>" style="width: 40px; height: 40px; display: inline-block;">
                            <?= Html::img(Html::encode(Url::to($item->fileUrl()).'?'.time()), ['style'=>'max-width: 100%; max-height: 100%;']); ?>
                        </a>
                    <?php endforeach ?>
                </div>

                <div class="col-xs-12 text-center">
                    <?= Html::a('Обратная связь', Url::to(['site/feedback']), ['class'=>'btn btn-warning btn-lg', 'style'=>'margin-top: 40px; max-width: 200px;']) ?>
                </div>

            </div>
        </div>

        <div class="" style="width: 100%;">
            <?php 
                if ($this->beginCache('ymap', ['duration' => 5])){
                    echo $this->render('_ymaps');
                    $this->endCache();
                }
            ?>
        </div>
    </div>
</div>