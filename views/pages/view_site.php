<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды спорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="section section-top">
    <div class="wrap flex-top">
        
        <div class="col-xs-12 text-center">
            <div class="h1"><?= Html::encode($this->title) ?></div>
        </div>

        <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="news-detail">
                
                <div class="news-detail-txt">
                    <?= $model->body ?>
                </div>
                <hr>

            </div>
        </div>
    
    </div>
</div>