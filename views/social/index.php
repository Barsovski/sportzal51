<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SocialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Социальные сети';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'alias',
            'url:url',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($model){
                    if( $model->image ){
                        return Html::img(
                                Html::decode(
                                    Url::to('@web' . $model::IMAGE_PATH . $model->image )
                                ).'?'.time(), ['style'=>'height: 18px;']
                            );
                    } 
                    else
                        return '';
                },
                'filter' => false,
                'headerOptions' => ['style' => 'width: 100px'],
                'contentOptions' => ['style' => 'width: 100px;', 'class'=>'text-center'],
            ],
            
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
