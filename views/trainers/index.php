<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TraindersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тренеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить тренера', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($model){
                    if( $model->image ){
                        return Html::img(
                                Html::decode(
                                    Url::to('@web' . $model::IMAGE_PATH . $model->image )
                                ), ['style'=>'height: 60px;']
                            );
                    } 
                    else
                        return '';
                },
                'filter' => false,
                'headerOptions' => ['style' => 'width: 100px'],
                'contentOptions' => ['style' => 'width: 100px;', 'class'=>'text-center'],
            ],
            'name',
            'alias',
            [
                'attribute' => 'on_main',
                'format' => 'raw',
                'value' => function($model){
                    if($model->on_main)
                        return '<span class="glyphicon glyphicon-ok" aria-hidden="false"></span>';
                    else
                        return '';
                },
                'contentOptions' => ['style' => 'width: 100px; font-size: 20px;', 'class'=>'text-center text-info'],
                'filter' => array(1 => 'На главной', 0 => 'Прочие' ),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
            ],
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-condensed'
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
