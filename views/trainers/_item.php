<?php
use yii\helpers\Url;
?>
<div class="trener trener3">
    <div class="trener-item">
        <div class="trener-text">
            <div class="trener-name"><?= $model->name ?></div>
            <div class="style"></div>
            <!-- <div class="team-prev">
                <div><?= $model->about ?></div>
            </div> -->
        </div>
        <img class="lazy" data-src="<?= $model->fileUrl() ?>" alt="<?= $model->name ?>" title="<?= $model->name ?>">
        <a class="trener-link btn btn-danger" href="<?= Url::toRoute(['trainers/show', 'alias'=>$model->alias]) ?>">Подробнее</a>
    </div>
</div>