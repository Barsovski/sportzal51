<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use app\models\Sports;

$sport = Sports::find()->where(['trainer_id'=>$model->id])->one();

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды спорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="section section-top">
    <div class="wrap">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 team-detail-photo pa-0">
                <img class="lazy img-responsive" data-src="<?= $model->fileUrl() ?>" alt="<?= $model->name ?>">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 team-detail-desc">
                <h1 class="team-detail-name trener"><?= $model->name ?></h1>
                <div class="team-detail-discipline">
                    <?php
                        if($sport){
                            echo Html::a($sport->name, Url::toRoute(['sports/show', 'alias'=>$sport->alias]));
                        }
                    ?>
                </div>
                <div class="detail-desc">
                    <?= $model->about ?>
                </div>
                <hr>
                <div class="text-left">
                    <a class="btn btn-danger" href="<?= Url::toRoute(['trainers/list']) ?>">Возврат к списку тренеров</a>
                </div>
            </div>
        </div>
    </div>
</div>