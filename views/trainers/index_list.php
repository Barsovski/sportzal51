<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ListView;

use app\models\Trainers;
use app\models\Sports;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = 'Список тренеров';
\yii\web\YiiAsset::register($this);

?>
<div class="section section-top section-full">
    
        <div class="news-list-v2">
            <div class="trener-box-list">
                
                <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_item',
                        'layout'=>"{pager}\n{items}",
                        'options'=>[
                            'style'=>'width: 100%; height: auto;',
                            'class' => 'trener-right',
                            'id' => false
                        ],
                        'itemOptions' => [
                            'tag' => false,
                        ],
                    ]); ?>

            </div>
        </div>
    
</div>