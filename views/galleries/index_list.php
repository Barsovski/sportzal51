<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

use app\models\Trainers;
use app\models\Sports;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = 'Список дисциплин';
\yii\web\YiiAsset::register($this);
$sports = Sports::find()->all();
?>
<div class="section section-top section-full">
    <div class="wrap wrap-full">
        
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'layout'=>"{items}\n<div class='col-xs-12'>{pager}</div>",
            'options'=>[
                'style'=>'width: 100%;',
                'class' => 'news-list-v2',
                'id' => false
            ],
            'itemOptions' => [
                'tag' => false,
            ],
        ]); ?>
    </div>
</div>