<?php
use yii\helpers\Url;
?>
<div class="news-item-v2">
    <a href="<?= $model->fileUrl() ?>" 
        data-lightbox="gallery-<?= $model->parent_id ?>" 
        data-title="<?= $model->name ?>"
        title="Нажмите для просмотра"
    >
        <div class="news-item-bg-v2 lazy" data-src="<?= $model->fileUrl() ?>"></div>
    </a>
</div>