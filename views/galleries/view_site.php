<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Sports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Виды спорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="section section-top section-full">
    <div class="wrap wrap-full">
        <div class="col-xs-12 text-center">
                <div class="h1">
                <?= $model->name ?><br>
                <small style="color:#F5F5F5;"><sup>Нажмите на изображение для увеличения</sup></small>
                </div>
            </div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_item_gallery_image',
            'layout'=>"{items}\n<div class='col-xs-12'>{pager}</div>",
            'options'=>[
                'style'=>'width: 100%;',
                'class' => 'news-list-v2',
                'id' => false
            ],
            'itemOptions' => [
                'tag' => false,
            ],
        ]); ?>
    </div>
</div>