<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Галереи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galleries-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить Галерею', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Галереи - Изображения', ['galleries-images/index'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            [
                'label' => 'Ссылка',
                'value' => function($model){
                    $url = Url::to(['galleries/show', 'alias'=>$model->alias]);
                    $url = explode('/', $url);
                    $url = join('/', array_splice($url, -3));
                    return '<code>'.$url.'</code>';
                    
                },
                'format' => 'html',
            ],
            'list_order',
            [
                'attribute' => 'visible',
                'value' => function($model){
                    if($model->visible == 1)
                        return 'Видим';
                    else
                        return 'Стрыт';
                    
                },
                'filter' => [1 => 'Видим', 0 => 'Скрыт'],
            ],
            
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
