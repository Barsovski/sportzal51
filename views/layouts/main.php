<?php
/* @var $this \yii\web\View */
/* @var $content string */
use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
// 
use app\assets\IndexAsset;
use app\models\MenuItems;
use app\models\Params;
use app\models\Social;
use app\models\Sports;
// 
$menu = MenuItems::find()->joinWith('parent')
                        ->orderBy([
                            'parent_id' => SORT_ASC,
                            'list_order' => SORT_ASC,
                            'name' => SORT_ASC
                        ])->all();
$social = Social::find()->all();
$logo_main = Params::findOne(['alias'=>'logo-main']);
$main_bg = Params::findOne(['alias'=>'main-bg-1']);
$site_name = Params::findOne(['alias'=>'site-name']);
$footer_icon = Params::findOne(['alias'=>'footer-icon']);
$contact_phone = Params::findOne(['alias'=>'contact-phone']);
$contact_address = Params::findOne(['alias'=>'contact-address']);
$sports = Sports::find()->joinWith('trainer')->all();

$color_body     = Params::findOne(['alias'=>'color_all_text_body']);
$color_a        = Params::findOne(['alias'=>'color_all_a_href_link_text']);
$trener_border  = Params::findOne(['alias'=>'class_trener_item_border_color']);
$trener_bg      = Params::findOne(['alias'=>'class_trener_item_background_color']);
$trener_color   = Params::findOne(['alias'=>'class_trener_item_text_color']);
$trener_color_view= Params::findOne(['alias'=>'class_trener_view_text_color']);
$news_border    = Params::findOne(['alias'=>'class_news_box_border_color']);
$news_bg        = Params::findOne(['alias'=>'class_news_box_background_color']);
$news_color     = Params::findOne(['alias'=>'class_news_box_text_color']);
$color_title    = Params::findOne(['alias'=>'class_title_page_text_color']);
$color_nav_a    = Params::findOne(['alias'=>'color_nav_a']);
$color_timetable= Params::findOne(['alias'=>'color_timetable_default']);
$color_timetable_font = Params::findOne(['alias'=>'color_timetable_color']);
$color_timetable_wd = Params::findOne(['alias'=>'color_timetable_wd_color']);
$color_footer   = Params::findOne(['alias'=>'color_footer']);
$color_main_bg  = Params::findOne(['alias'=>'color_bg_main']);

$sport_color    = Params::findOne(['alias'=>'class_sport_color']);

$header_color_bg= Params::findOne(['alias'=>'class_header_color_bg']);
$header_color_main_link = Params::findOne(['alias'=>'header_color_main_link']);

$main_color_adv = Params::findOne(['alias'=>'style_main_color_adv']);

$price_color_text  = Params::findOne(['alias'=>'style_price_text_color']);
$price_color_back  = Params::findOne(['alias'=>'style_price_background_color']);
$price_color_border  = Params::findOne(['alias'=>'style_price_border_color']);

$footer_text      = Params::findOne(['alias'=>'footer_text']);
$footer_text_size = Params::findOne(['alias'=>'footer_text_size']);

$url_base = Url::toRoute('/');
$url_current = Url::current();

IndexAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= $site_name->val_text . ' - ' . Html::encode($this->title) ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="index, follow" />
    <?php $this->head() ?>
    <style>
        body{
            <?php if($color_body): ?>
                color: <?= $color_body->val_text ?>;
            <?php endif; ?>
            <?php if($color_main_bg): ?>
                background: <?= $color_main_bg->val_text; ?>;
            <?php endif; ?>
        }
        <?php if($color_a): ?>
            a{color: <?= $color_a->val_text ?>;}
        <?php endif; ?>
        .trener-item{
            border-radius: 1px;
            <?php if($trener_border): ?>
                border: 2px solid <?= $trener_border->val_text ?>;
            <?php endif; ?>
            <?php if($trener_bg): ?>
                background: <?= $trener_bg->val_text ?>;
            <?php endif; ?>
        }
        <?php if($trener_color): ?>
            .trener-item .trener-name{color: <?= $trener_color->val_text ?>;}
        <?php endif; ?>
        .news-box{
            border-radius: 1px;
            <?php if($news_border): ?>
                border: 2px solid <?= $news_border->val_text ?>;
            <?php endif; ?>
            <?php if($news_bg): ?>
                background: <?= $news_bg->val_text ?>;
            <?php endif; ?>
            <?php if($news_color): ?>
                color: <?= $news_color->val_text ?>;
            <?php endif; ?>
        }
        <?php if($sport_color): ?>
            .news-item-name-v2{color: <?= $sport_color->val_text ?>;}
        <?php endif; ?>
        <?php if($color_title): ?>
            .title-page{color: <?= $color_title->val_text ?>;}
        <?php endif; ?>
        <?php if($color_nav_a): ?>
            .nav ul li ul li a{color: <?= $color_nav_a->val_text ?> !important;}
        <?php endif; ?>
        <?php if($color_timetable): ?>
            .timetable-item{
                background:  <?= $color_timetable->val_text ?>;
            }
        <?php endif; ?>
        .timetable-item-hall{
            /* margin-top: -3px;  */
            /* margin-bottom:-5px; */
        }
        <?php if($color_timetable_font): ?>
            .timetable-item a, .timetable-item-hall { color: <?= $color_timetable_font->val_text ?>; }
            .timetable-item-discipline{ color: <?= $color_timetable_font->val_text ?>; }
        <?php endif; ?>
        .price-item-box{
            <?php if($price_color_back): ?>
                background: <?= $price_color_back->val_text ?>;
            <?php endif; ?>
            <?php if($price_color_text): ?>
                color: <?= $price_color_text->val_text ?>;
            <?php endif; ?>
            <?php if($price_color_border): ?>
                border: 2px solid <?= $price_color_border->val_text ?>;
            <?php endif; ?>
        }

        body{
            <?php if($color_body): ?>
                color: <?= $color_body->val_text ?>;
            <?php endif ?>
            <?php if($color_main_bg): ?>
                background-color: <?= $color_main_bg->val_text; ?>;
            <?php endif ?>
            <?php if($main_bg): ?>
                background-image: url(<?= $main_bg->fileUrl(); ?>);
            <?php endif ?>
            background-repeat: no-repeat;
            background-position: bottom left;
            background-size: cover;
            background-attachment: fixed;
        }

        footer{ 
            <?php if($color_footer): ?>
                background-color: <?= $color_footer->val_text; ?> ;
            <?php endif; ?>
            <?php if($footer_text_size): ?>
                font-size: <?= $footer_text_size->val_text; ?>px;
            <?php endif; ?>
        }

        header{ background: <?= $header_color_bg->val_text; ?> }

        .btn-header{color: <?= $main_color_adv->val_text ?> }
        .nav ul li a{color: <?= $header_color_main_link->val_text ?>}
        .nav ul li a:hover, header nav ul li.active a{color: <?= $main_color_adv->val_text ?>; }
        .nav ul li ul{background: <?= $main_color_adv->val_text ?>; }
        .nav ul li ul .triangle::after{
            border-left: 200px solid <?= $main_color_adv->val_text ?>;
            border-top: 0 solid <?= $main_color_adv->val_text ?>;
        }
        .section-title::after{background: <?= $main_color_adv->val_text ?> }
        .section-title a:hover{border-color: <?= $main_color_adv->val_text ?> }

        .red{color: <?= $main_color_adv->val_text ?> }
        .tabs__caption2 li.active{color: <?= $main_color_adv->val_text ?> }
        .week-name{
            background: <?= $main_color_adv->val_text ?>;
            color: <?= $color_timetable_wd->val_text ?>;
        }
        .team-detail-name.trener{
            color: <?= $trener_color_view->val_text ?>;
        }
    </style>
</head>

<body class="">
    <?php $this->beginBody() ?>
    <?php if( $url_base == $url_current): ?>
        <div id="page">
    <?php else: ?>
        <div id="page" style="height: auto">
    <?php endif ?>
        <header class="header">
            <div class="wrap flex-row">
                <div class="item flex-row">
                    <a href="#menu" class="gamburger"></a>
                    <a href="<?= Url::to(['site/index']) ?>"><img class="logo" src="<?= $logo_main->fileUrl() ?>" alt="" title=""></a>
                    <nav class="nav">
                        <ul>
                            <?php
								foreach ($menu as $item) {
									if($item->parent_id == 0){
                                        echo '<li class="include">';
                                        if($item->link!==''){
                                            echo '<a href="'. Url::to([$item->link]) .'">'.$item->name.'</a>';
                                        }
                                        else{
                                            echo '<a href="javascript:void(0)">'.$item->name.'</a>';
                                        }
										if( $item->hasChildIn($menu) ) 
										{
											echo '<ul>';
											foreach ($menu as $item2) 
											{
												if($item2->parent && $item2->parent_id == $item->id)
													echo '<li><a href="'. Url::to([$item2->link]) .'">'.$item2->name.'</a></li>';
											}
											echo '<div class="triangle"></div>';
											echo '</ul>';
                                        }
                                        else if ($item->alias == 'sports'){
                                            echo '<ul>';
                                            foreach ($sports as $sitem) {
                                                echo '<li><a href="'. Url::toRoute(['sports/show', 'alias'=>$sitem->alias]) .'">'.$sitem->name.'</a></li>';
                                            }
                                            echo '<div class="triangle"></div>';
                                            echo '</ul>';
                                        }
									}
								}
							?>
                        </ul>
                    </nav>
                </div>
                <div class="item">
                    <!-- <a data-fancybox="" data-type="ajax" data-src="/ajax/order.php" href="javascript:;" class="btn-header">Заказать звонок</a> -->
                    <a href="<?= Url::to(['site/feedback']) ?>" class="btn-header">Заказать звонок</a>
                    <strong><?= $contact_phone->val_text ?></strong>
                </div>
                <div class="social">
                    <?php
						foreach ($social as $item) {
							print('<a href="' . $item->url . '" title="' . $item->name . '" target="_blank" rel="nofollow"><img src="' . $item->fileUrl() .'?'. time() . '" alt=""></a>');
						}
					?>
                </div>
                <div class="title-page"><?= $site_name->val_text . ' - ' . $this->title ?></div>
            </div>
        </header>

        <?= $content ?>
        
        <footer>
            <div class="wrap flex-row">
                <div class="flex-row">
                    <img class="footer-symbol" src="<?= $footer_icon->fileUrl(); ?>">
                    <div class="item">
                        <div class="email-title">
                            <?= $footer_text->val_text; ?>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <!-- <div class="copy">© Anri Barsovski, 2019</div> -->
                </div>
            </div>
        </footer>
    </div>

    <?php $this->endBody() ?>
    
    <nav id="menu">
        <ul>
            <?php
                foreach ($menu as $item) {
                    if($item->parent_id == 0){
                        echo '<li class="include">';
                        if($item->link!==''){
                            echo '<a href="'. Url::to([$item->link]) .'"><span>'.$item->name.'</span></a>';
                        }
                        else{
                            echo '<span>'.$item->name.'</span>';
                        }
                        echo "\n\r";
                        if( $item->hasChildIn($menu) ) 
                        {
                            echo '<ul>';
                            foreach ($menu as $item2) 
                            {
                                if($item2->parent && $item2->parent_id == $item->id){
                                    echo '<li><a href="'. Url::to([$item2->link]) .'">'.$item2->name.'</a></li>';
                                    echo "\n\r";
                                }
                            }
                            echo '</ul>';
                        }
                        else if ($item->alias == 'sports'){
                            echo '<ul>';
                            foreach ($sports as $sitem) {
                                echo '<li><a href="'. Url::toRoute(['sports/show', 'alias'=>$sitem->alias]) .'">'.$sitem->name.'</a></li>';
                                echo "\n\r";
                            }
                            echo '</ul>';
                            echo "\n\r";
                        }
                    }
                }
            ?>
        </ul>
      </nav>
	<script>
        $(document).ready(function(){
            setMmenu("<?= $site_name->val_text ?>");
        });
    </script>
</body>

</html>
<?php $this->endPage() ?>