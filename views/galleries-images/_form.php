<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Galleries;

/* @var $this yii\web\View */
/* @var $model app\models\GalleriesImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="galleries-images-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(
        ArrayHelper::map(Galleries::find()->all(),'id','name'),
        ['prompt'=>'Выберите Галерею']
    ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= $form->field($model, 'image')->fileInput() ?>
        <?php
            if($model->image){

                echo Html::img(
                        Html::encode(
                            Url::to('@web' . $model::FILE_PATH . $model->image )
                        ), ['style'=>'max-height: 200px;']
                    );
            }
        ?>
    </div>

    <?= $form->field($model, 'visible')->DropDownList([1 => 'Показывать', 0 => 'Не показывать']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
