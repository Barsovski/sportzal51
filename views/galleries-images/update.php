<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GalleriesImages */

$this->title = 'Редактирование: ' . $model->id . ' из галереи [' . $model->parent->name . ']';
$this->params['breadcrumbs'][] = ['label' => 'Галереи - изображения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="galleries-images-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
