<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Galleries;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Галереи - изображения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galleries-images-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить изображение галереи', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Галереи', ['galleries/index'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            'name',
            [
                'attribute' => 'parent_id',
                'value' => 'parent.name',
                'filter' => ArrayHelper::map(Galleries::find()->all(), 'id', 'name'),
            ],
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($model){
                    if( $model->image ){
                        $string = Html::img(
                                Html::decode(
                                    Url::to('@web' . $model::FILE_PATH . $model->image )
                                ), ['style'=>'height: 100px;']
                            );
                        return $string;
                    }
                },
                'filter' => false,
                'contentOptions' => ['nowrap'=>''],           
            ],
            [
                'attribute' => 'visible',
                'value' => function($model){
                    if($model->visible == 1)
                        return 'Видим';
                    else
                        return 'Стрыт';
                    
                },
                'filter' => [1 => 'Видим', 0 => 'Скрыт'],
            ],
            
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
