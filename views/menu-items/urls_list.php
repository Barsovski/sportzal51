<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\News;
use app\models\Pages;
use app\models\Galleries;
use app\models\Trainers;
use app\models\Sports;
use app\models\Schedule;

?>

<div class="row">
    <div class="col-xs-12">
        <hr style="border-color: #AAA; border-width: 2px; border-style: solid; width: 100%; margin-top: 10px;">
        <h2 style="margin-top: 0px;">Основные ссылки для меню</h2>
    </div>
    <div class="col-xs-12 col-md-6">
        <label>Страницы сайта</label>
        <table class="table table-condensed">
            <tr><td>Главная</td><td><code><?= clearUrl(Url::to(['site/index'])) ?></code></td></tr>
            <tr><td>Контакты</td><td><code><?= clearUrl(Url::to(['site/contact'])) ?></code></td></tr>
            <tr><td>Обратная связь</td><td><code><?= clearUrl(Url::to(['site/feedback'])) ?></code></td></tr>
            <tr><td>Цены</td><td><code><?= clearUrl(Url::to(['price-list/page'])) ?></code></td></tr>
        </table>
    </div>
    <div class="col-xs-12 col-md-6">
        <label>Страницы</label>
        <table class="table table-condensed">
            <?php foreach (Pages::find()->all() as $item): ?>
                <tr><td><?= $item->name ?></td><td><code><?= clearUrl(Url::to(['pages/show', 'alias'=>$item->alias])) ?></code></td></tr>
            <?php endforeach ?>
        </table>
    </div>
    <div class="col-xs-12 col-md-6">
        <label>Расписание</label>
        <table class="table table-condensed">
            <tr><td>Страница расписаний</td><td><code><?= clearUrl(Url::to(['schedule/list'])) ?></code></td></tr>
        </table>
    </div>
    <div class="col-xs-12 col-md-6">
        <label>Галереи</label>
        <table class="table table-condensed">
            <?php foreach (Galleries::find()->all() as $item): ?>
                <tr><td><?= $item->name ?></td><td><code><?= clearUrl(Url::to(['galleries/show', 'alias'=>$item->alias])) ?></code></td></tr>
            <?php endforeach ?>
        </table>
    </div>
    <div class="col-xs-12 col-md-6">
        <label>Тренеры</label>
        <table class="table table-condensed">
            <tr><td>Список тренеров</td><td><code><?= clearUrl(Url::to(['trainers/list'])) ?></code></td></tr>
            <?php foreach (Trainers::find()->all() as $item): ?>
                <tr><td><?= $item->name ?></td><td><code><?= clearUrl(Url::to(['trainers/show', 'alias'=>$item->alias])) ?></code></td></tr>
            <?php endforeach ?>
        </table>
    </div>
    <div class="col-xs-12 col-md-6">
        <label>Виды спорта</label>
        <table class="table table-condensed">
            <tr><td>Список видов спорта</td><td><code><?= clearUrl(Url::to(['sports/list'])) ?></code></td></tr>
            <?php foreach (Sports::find()->all() as $item): ?>
                <tr><td><?= $item->name ?></td><td><code><?= clearUrl(Url::to(['sports/show', 'alias'=>$item->alias])) ?></code></td></tr>
            <?php endforeach ?>
        </table>
    </div>
    <div class="col-xs-12 col-md-6">
        <label>Новости</label>
        <table class="table table-condensed">
            <tr><td>Список новостей</td><td><code><?= clearUrl(Url::to(['news/list'])) ?></code></td></tr>
            <?php foreach (News::find()->all() as $item): ?>
                <tr><td><?= $item->name ?></td><td><code><?= clearUrl(Url::to(['news/show', 'alias'=>$item->alias])) ?></code></td></tr>
            <?php endforeach ?>
        </table>
    </div>
</div>