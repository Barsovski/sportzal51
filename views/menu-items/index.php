<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\MenuItems;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пункты меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-items-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить пункт меню', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'link',
            [
                'attribute' => 'parent_id',
                'value' => 'parent.name',
                'filter' => ArrayHelper::map(MenuItems::find()->all(), 'id', 'name'),
            ],
            'list_order',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
            ],
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-condensed'
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
