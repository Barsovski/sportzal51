<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\MenuItems */

$this->title = 'Редактирование пункта: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пункты меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
function clearUrl($url){
    $remove = Url::to(['site/index']);
    if($remove === $url)
        return '/';
    else
        return str_replace($remove, '', $url);
}
?>
<div class="menu-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?= $this->render('urls_list'); ?>
    
</div>
