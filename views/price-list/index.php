<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\models\PricePeriods;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PriceListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цены - список цен';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-list-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Цены - Периоды', ['price-periods/index'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'price_period_id',
                'value' => 'pricePeriod.name',
                'filter' => ArrayHelper::map(PricePeriods::find()->all(), 'id', 'name'),
            ],
            'name',
            // 'text',
            'price',
            'list_order',
            [
                'attribute' => 'visible',
                'value' => function($model){
                    if($model->visible == 1)
                        return 'Видим';
                    else
                        return 'Стрыт';
                    
                },
                'filter' => [1 => 'Видим', 0 => 'Скрыт'],
            ],
            
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 80px;', 'class'=>'text-center'],
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
