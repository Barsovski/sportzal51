<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use app\models\PricePeriods;
/* @var $this yii\web\View */
/* @var $model app\models\PriceList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'price_period_id')->dropDownList(
        ArrayHelper::map(PricePeriods::find()->all(),'id','name'),
        ['prompt'=>'Выберите родительский пункт меню']
    ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'list_order')->textInput() ?>

    <?= $form->field($model, 'visible')->DropDownList([1 => 'Показывать', 0 => 'Не показывать']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
