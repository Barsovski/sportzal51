<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;

use yii\widgets\ActiveForm;

// use app\models\Schedule;
$this->title = 'Цены';
?>
<div class="section section-top">
    <div class="wrap">
        <div class="tabs">
            <div class="tabs__content active">
                <div class="col-xs-12 text-center">
                    <div class="h1 mb-0"><?= Html::encode($this->title) ?></div>
                </div>

                <div class="tabs2">
                    <?php if(count(@$periods)>0): ?>
                        <ul class="tabs__caption2">
                            <?php foreach ($periods as $key => $item): ?>
                                <li class="<?php if ($key == 0): ?>active<?php endif; ?>">
                                    <span><?= $item->name ?></span>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        
                        <?php foreach ($periods as $key => $item): ?>
                            <div class="tabs__content2 <?php if ($key == 0): ?>active<?php endif; ?>">
                                <div class="price-table">
                                    <?php foreach ($lists as $pkey => $price): ?>
                                        <?php if($price->price_period_id == $item->id): ?>
                                            <div class="price-item">
                                                <div class="price-item-box">
                                                    <div class="text-center"><span class="glyphicon glyphicon-download"></span></div>
                                                    <div class="price-item-name"><?= $price->name ?></div>
                                                    <div class="price-item-desc"><?= $price->text ?></div>
                                                    <div class="price-item-bottom">
                                                        <p class="red"><?= $price->price ?> ₽</p>
                                                    </div>
                                                </div>
                                            </div>		
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                                    
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>