<?php

namespace app\controllers;

use Yii;
use app\models\Schedule;
use app\models\ScheduleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Inflector;

use app\models\ScheduleForm;
use app\models\Sports;
use app\models\Trainers;

/**
 * ScheduleController implements the CRUD actions for Schedule model.
 */
class ScheduleController extends Controller
{
    public function init() {
        parent::init();
        $this->layout = 'admin';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['page'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Schedule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScheduleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Schedule model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Schedule model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPage()
    {
        $this->layout = 'main';

        $schedules = Schedule::find()->joinWith(['sport', 'trainer t']);
        $filter_form = new ScheduleForm();

        $days = [
            1=>'Понедельник',
            2=>'Вторник',
            3=>'Среда',
            4=>'Четверг',
            5=>'Пятница',
            6=>'Суббота',
            7=>'Воскресенье'
        ];

        if (Yii::$app->request->isAjax && $filter_form->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPjax) {
                if($filter_form->validate()){
                    if($filter_form->trainer_id)
                        $schedules = $schedules->andWhere([Schedule::tableName().'.trainer_id'=>$filter_form->trainer_id]);
                    if($filter_form->sport_id)
                        $schedules = $schedules->andWhere([Schedule::tableName().'.sport_id'=>$filter_form->sport_id]);
                    if($filter_form->hall)
                        $schedules = $schedules->andWhere([Schedule::tableName().'.hall'=>$filter_form->hall]);
                }
                $schedules = $schedules->all();
                $array_ = [];
                foreach ($schedules as $item) {
                    array_push($array_, $item->time_s);
                }
                $times_array = array_unique($array_);
                asort($times_array);
                return $this->render('_schedule_block', [
                    'days'=>$days,
                    'times_array'=>$times_array,
                    'schedules'=>$schedules,
                ]);
            } 
            elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }
        
        $schedules = $schedules->all();
        $array_ = [];
        foreach ($schedules as $item) {
            array_push($array_, $item->time_s);
        }
        $times_array = array_unique($array_);
        asort($times_array);
        
        return $this->render('schedule', [
            'schedules' => $schedules,
            'times_array' => $times_array,
            'days' => $days,
            'filter_form' => $filter_form,

            'sports' => Sports::find()->all(),
            'trainers' => Trainers::find()->all(),
        ]);
    }

    /**
     * Creates a new Schedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedule();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Schedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Schedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedule::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
