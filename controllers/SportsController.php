<?php

namespace app\controllers;

use Yii;
use app\models\Sports;
use app\models\SportsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Inflector;

/**
 * SportsController implements the CRUD actions for Sports model.
 */
class SportsController extends Controller
{
    public function init() {
        parent::init();
        $this->layout = 'admin';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['show', 'list'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Sports models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SportsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Sports models.
     * @return mixed
     */
    public function actionList()
    {
        $searchModel = new SportsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = 'main';
        return $this->render('index_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sports model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Sports model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionShow($alias)
    {
        $this->layout = 'main';
        return $this->render('view_site', [
            'model' => $this->findModel($alias),
        ]);
    }

    /**
     * Creates a new Sports model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sports();

        $IMAGES_FOLDER = Yii::getAlias('@webroot/' . $model::IMAGE_PATH);

        if ($model->load(Yii::$app->request->post())) {
            if(empty($model->alias))
                $model->alias = Inflector::slug($model->name, $replacement = '_', $lowercase = true);
            if($model->validate()){
                $model->image = UploadedFile::getInstance($model, 'image');
                if(!empty($model->image)){
                    $image_name = time() . '_' . $model->alias . '.' . $model->image->extension;
                    $model->image->saveAs($IMAGES_FOLDER . $image_name);
                    $model->image = $image_name;
                }
                if($model->save()){
                    return $this->redirect(['index']);
                };
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sports model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $IMAGES_FOLDER = Yii::getAlias('@webroot/' . $model::IMAGE_PATH);

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                if(empty($model->alias))
                    $model->alias = Inflector::slug($model->name, $replacement = '_', $lowercase = true);
                $model->image = UploadedFile::getInstance($model, 'image');
                if(!empty($model->image)){
                    @unlink($IMAGES_FOLDER . $image_);
                    $image_name = time() . '_' . $model->alias . '.' . $model->image->extension;
                    $model->image->saveAs($IMAGES_FOLDER . $image_name);
                    $model->image = $image_name;
                }
                else{
                    $model->image = $image_;
                }
                if($model->save()){
                    return $this->redirect(['index']);
                };
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sports model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        
        if( $model->image!==''){
            $IMAGES_FOLDER = Yii::getAlias('@webroot/' . $model::IMAGE_PATH);
            @unlink($IMAGES_FOLDER . $model->image);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sports model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sports the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Sports::find()->andWhere(['id'=>$id])->orWhere(['alias'=>$id])->one();
        if( $model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
