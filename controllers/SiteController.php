<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Feedback;

use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'admin'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['admin'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                // 'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor'=>0x85222f,
                'transparent'=>true,
                'backColor'=>0x0000000,
                'height'=>50,
                'width'=>150,
                // 'testLimit'=>0,
                'fontFile' => '@webroot/fonts/RobotoMono-Regular.ttf',

            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'feedback_model'=>new Feedback(),
        ]);
    }

    public function actionAdmin()
    {
        $this->layout = 'admin';
        return $this->render('admin');
    }

    // public function actionAddAdmin() {
    //     $model = User::find()->where(['username' => 'admin'])->one();
    //     if (empty($model)) {
    //         $user = new User();
    //         $user->username = 'global admin';
    //         $user->email = 'bars.work@bk.ru';
    //         $user->setPassword('adminadmin');
    //         $user->generateAuthKey();
    //         if ($user->validate()) {
    //             $user->save();
    //             echo 'good';
    //         }
    //         else{
    //             print_r($user->getErrors());
    //         }
    //     }
    // }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'admin_clear';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        return $this->render('/site/contact', []);
    }

    /**
     * Displays feedback page.
     *
     * @return Response|string
     */
    public function actionFeedback()
    {
        $model = new Feedback();
        
        return $this->render('/site/feedback', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
