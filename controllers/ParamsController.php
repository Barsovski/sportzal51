<?php

namespace app\controllers;

use Yii;
use app\models\Params;
use app\models\ParamsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Inflector;

/**
 * ParamsController implements the CRUD actions for Params model.
 */
class ParamsController extends Controller
{
    public function init() {
        parent::init();
        $this->layout = 'admin';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Params models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParamsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Params models.
     * @return mixed
     */
    public function actionRendercss()
    {
        $query = Params::find()->where([
            'like', 'name', 'STYLE'
        ]);
        // $query = $query->andWhere([

        // ]);

        return $this->render('render_css', [
            'parameters' => $query->all(),
        ]);
    }

    /**
     * Displays a single Params model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Params model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Params();
        $FILES_FOLDER = Yii::getAlias('@webroot/' . $model::FILE_PATH);

        if ($model->load(Yii::$app->request->post())) {
            if(empty($model->alias))
                $model->alias = Inflector::slug($model->name, $replacement = '_', $lowercase = true);
            if($model->validate()){
                $model->val_file = UploadedFile::getInstance($model, 'val_file');
                if(!empty($model->val_file)){
                    $val_file_name = time() . '_' . $model->alias . '.' . $model->val_file->extension;
                    $model->val_file->saveAs($FILES_FOLDER . $val_file_name);
                    $model->val_file = $val_file_name;
                }
                if($model->save()){
                    return $this->redirect(['index']);
                };
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Params model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $file_ = $model->val_file;
        $FILES_FOLDER = Yii::getAlias('@webroot/' . $model::FILE_PATH);

        if ($model->load(Yii::$app->request->post())) {
            $clearimage = Yii::$app->request->post('clearimage');

            if(empty($model->alias))
                $model->alias = Inflector::slug($model->name, $replacement = '_', $lowercase = true);
            if($model->validate()){
                $model->val_file = UploadedFile::getInstance($model, 'val_file');
                if(!empty($model->val_file)){
                    @unlink($FILES_FOLDER . $file_);
                    $val_file_name = time() . '_' . $model->alias . '.' . $model->val_file->extension;
                    $model->val_file->saveAs($FILES_FOLDER . $val_file_name);
                    $model->val_file = $val_file_name;
                }
                else{
                    $model->val_file = $file_;
                }
                if( $clearimage && $clearimage!="" ){
                    @unlink($FILES_FOLDER . $model->val_file);
                    $model->val_file = '';
                }
                if($model->save()){
                    return $this->redirect(['index']);
                };
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Params model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        
        if( $model->val_file!==''){
            $FILES_FOLDER = Yii::getAlias('@webroot/' . $model::FILE_PATH);
            @unlink($FILES_FOLDER . $model->val_file);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Params model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Params the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Params::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
