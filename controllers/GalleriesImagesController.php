<?php

namespace app\controllers;

use Yii;
use app\models\GalleriesImages;
use app\models\GalleriesImagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Inflector;

/**
 * GalleriesImagesController implements the CRUD actions for GalleriesImages model.
 */
class GalleriesImagesController extends Controller
{
    public function init() {
        parent::init();
        $this->layout = 'admin';
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['show', 'list'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],  
        ];
    }

    /**
     * Lists all GalleriesImages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GalleriesImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GalleriesImages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GalleriesImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GalleriesImages();

        $FILES_FOLDER = Yii::getAlias('@webroot/' . $model::FILE_PATH);

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                $model->image = UploadedFile::getInstance($model, 'image');
                if(!empty($model->image)){
                    $image_name = $model->parent_id . '_' . time() . '.' . $model->image->extension;
                    $model->image->saveAs($FILES_FOLDER . $image_name);
                    $model->image = $image_name;
                }
                else{
                    $model->image = $file_;
                }
                if($model->save()){
                    return $this->redirect(['index']);
                };
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing GalleriesImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $file_ = $model->image;
        $FILES_FOLDER = Yii::getAlias('@webroot/' . $model::FILE_PATH);

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                $model->image = UploadedFile::getInstance($model, 'image');
                if(!empty($model->image)){
                    @unlink($FILES_FOLDER . $file_);
                    $image_name = $model->parent_id . '_' . time() . '.' . $model->image->extension;
                    $model->image->saveAs($FILES_FOLDER . $image_name);
                    $model->image = $image_name;
                }
                else{
                    $model->image = $file_;
                }
                if($model->save()){
                    return $this->redirect(['index']);
                };
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing GalleriesImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GalleriesImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GalleriesImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GalleriesImages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
