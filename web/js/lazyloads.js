/**
 * Функция для отложенной подгрузки изображений
 * Используется jQuery.lazy - http://jquery.eisbehr.de/lazy/
 * @param debug - js boolean, нужен для отражения дополнительной информации (:9)
 * Необходимо подключить скрипты jQuery.lazy до использования
 * Промайсы обеспечивают подгрузку изображений из кэша браузера (:13)
 */
function imagesSetLazy(debug = false) {
    if (debug)
        console.log('set Lazy imgs');

    if (window.caches) {
        const lazyImages = Array.prototype.slice.call(document.querySelectorAll('[data-src].lazy'));

        Promise.all(lazyImages.map(img => {
            const src = img.getAttribute('data-src');

            // Проверяем, есть ли в кэше ответ для изображения
            return window.caches.match(src).
            then(response => {
                if (response) {
                    // Нашли в кэше, прописываем
                    img.setAttribute('src', src);
                    img.setAttribute('alt', img.getAttribute('data-alt'));
                    img.removeAttribute('data-src');
                    img.removeAttribute('data-alt');
                }
            })
        })).
        then(RunlazyLoad(debug = debug)); // Отдали кэш, lazy load остальные изображения
    } else {
        // Кэш отключен или не поддерживается - lazy load на все картинки
        RunlazyLoad(debug = debug);
    }

}

/**
 * Wrapper для обёртывания в .Lazy()
 * Используется в функции imagesSetLazy() выше (:8)
 * @param debug - js boolean, нужен для отражения дополнительной информации (:44)
 */
function RunlazyLoad(debug = false) {
    $('[data-src].lazy').Lazy({
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        effectTime: 500,
        visibleOnly: true,
        enableThrottle: true,
        throttle: 250,
        threshold: 0,
        onError: function(element) {
            if (debug) {
                console.log('lazy load error loading ' + element.data('src'));
            }
            // element.attr('src', '/static/img/no_image.png');
            // if(element.hasClass('idea_img'))
            // element.toggleClass('idea_img ppo_blazon')
        },
        beforeLoad: function(element) {
            if (debug) {
                console.log('lazy load element', element);
            }
        }
    });
}

/**
 * Иницициализируем подгрузку всех lazy картинок и прочего
 * при загрузке страницы
 */
$(document).ready(function() {
    imagesSetLazy();
});