   var swiper_gallery = new Swiper('.swiper-gallery', {

   });
	
  $('.button-prev').click(function(){
	swiper_gallery.slidePrev();
			return false;		 
  });
  $('.button-next').click(function(){
	swiper_gallery.slideNext();
			return false;		 
  });	
  
    var swiper_prod = new Swiper('.swiper-prod', {
      slidesPerView: 3,
      spaceBetween: 20,
	  breakpoints: {

		350: {
		  slidesPerView: 1,
		  spaceBetween: 10
		},

		600: {
		  slidesPerView: 2,
		  spaceBetween: 10
		},

		800: {
		  slidesPerView: 3,
		  spaceBetween: 10
		}
	 }  
    });
	
   $('.prod-button-prev').click(function(){
	swiper_prod.slidePrev();
			return false;		 
  });
  $('.prod-button-next').click(function(){
	swiper_prod.slideNext();
			return false;		 
  });	
	
    var swiper_clients = new Swiper('.swiper-clients', {
	  slidesPerView: 5,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
	  breakpoints: {

		350: {
		  slidesPerView: 1,
		  spaceBetween: 10
		},

		500: {
		  slidesPerView: 2,
		  spaceBetween: 10
		},

		800: {
		  slidesPerView: 3,
		  spaceBetween: 10
		}
	  }
    });
	
   $('.button-prev').click(function(){
	swiper_clients.slidePrev();
			return false;		 
  });
  $('.button-next').click(function(){
	swiper_clients.slideNext();
			return false;		 
  });	
		