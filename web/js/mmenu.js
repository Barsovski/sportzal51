function setMmenu(title) {
    $("#menu").mmenu({
        extensions: ["shadow-panels", "fx-panels-slide-100", "theme-black", "pagedim-black"],
        navbar: {
            title: title,
        },
        navbars: {
            content: ["prev", "title"],
            height: 2
        },
        // setSelected: true,
        // searchfield: {
        //     resultsPanel: true
        // }
    }, {});
    $(".mh-head.mm-sticky").mhead({
        scroll: {
            hide: 200
        }
    });
    $(".mh-head:not(.mm-sticky)").mhead({
        scroll: false
    });



    $('body').on('click',
        'a[href^="#/"]',
        function() {
            alert("Thank you for clicking, but that's a demo link.");
            return false;
        }
    );
}