$(document).ready(function() {

    function easyscroll(elem) {
        event.preventDefault();

        var id = $(elem).attr('href'),

            top = $(id).offset().top;
        var h_height = $("header").outerHeight(true);

        $('body,html').animate({ scrollTop: (top) }, 1500);
    };

    $(".section-nav").on("click", "a", function(event) {
        easyscroll(this);
    });

    //навигация
    $(".menu-opener").click(function() {
        $(".menu-opener, .menu-opener-inner").toggleClass("active");
        $(".menu-overlay").toggleClass("menu-overlay-active");
    });

    $(".menu-opener").click(function() {
        $(".mobile-menu-block").toggleClass("menu-open");
    });

    $(".mobile-menu-block li").click(function() {
        $(".mobile-menu-block").removeClass("menu-open");
        $(".menu-opener, .menu-opener-inner").removeClass("active");
        $(".menu-overlay").removeClass("menu-overlay-active");
    });

    $(window).scroll(function() {
        var top = $("body").scrollTop();
        var top2 = $("html").scrollTop();
        if ((top > 0) || (top2 > 0)) {
            $("header").addClass("header-fixed");
        } else {
            $("header").removeClass("header-fixed");
        }
    })

    lightbox.option({
        'albumLabel': "Изображение %1 из %2"
    })

});